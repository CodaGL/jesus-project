#pragma once
#include "LevelData.h"
#include <functional>
#include "LightEntity.h"


namespace jet 
{

	class Entity;
	class Player;
	class Mob;

	enum class ECollisionFlags : uint32_t;

	struct EntityCollisionData
	{
		Entity* collided;
		jet::ECollisionFlags flags; //Where did we collided?
	};

	class Level
	{

	private:
		LevelData & m_levelData;
		jet::Player& m_player;

		std::vector<std::vector<jet::Entity*>> m_entities;
		std::vector<std::vector<jet::Entity*>> m_entitiesInTile;


		//std::vector<jet::LightEntity*> m_lights;

	public:

		Level(LevelData& data, jet::Player& player);
		~Level();


		void update(const sf::Time& time);
		void tick();
		void postTick();
		
		//void updateObjects(jet::EntityType et);

		void processCollisions();

		//void render(sf::RenderTarget& target, sf::RenderStates states);


		jet::TileData getTile(int x, int y) const;

		bool isCollidableAt(int x, int y) const;
		bool isLadder(int x, int y) const;

		bool isInBounds(int x, int y) const;

		//TODO: this should be embeed with @add. HACKY
		void addEquipment(jet::EntityEquipment* eq);

		void add(jet::Entity* entity);
		void remove(jet::Entity* entity);

		void addDamage(/*DamageType type*/jet::Entity* source, float x, float y, float width, float height, int time = 10, int damage = 1);

		void insertEntity(int tx, int ty, jet::Entity* entity);
		void removeEntity(int tx, int ty, jet::Entity* entity);

		std::vector<jet::Entity*> getEntitiesInTile(int tx, int ty);

		std::vector<jet::Entity*>& getEntitiesOfType(jet::EntityType et);

		/*			Test		*/
		typedef void(*vToCallFunction)();
		void forEachVisibleEntity(vToCallFunction a) {};
		/*			Test		*/

		std::vector<jet::Entity*> getEntitiesInRange(int tx, int ty, int rx, int ry);

		sf::Vector2f handleCollision(const sf::Vector2f& bodyPosition, float sz_x, float sz_y, float v_x, float v_y, jet::ECollisionFlags* collisionFlags = 0);

		std::vector<jet::EntityCollisionData> handleEntityCollision(jet::Entity* entity, sf::Vector2f& processed);

		sf::Vector2f handleMobCollisions(jet::Mob& toProcess);

		jet::Player& getPlayer()
		{
			return m_player;
		}

	};
}
