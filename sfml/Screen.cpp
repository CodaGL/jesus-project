#include "Screen.h"
#include "ScreenManager.h"

namespace jet
{
	void Screen::setNextScreen(Screen* screen)
	{
		if (screen == this)
			return;
		m_screenManager->setNextScreen(screen);
	}

	void Screen::setScreenManager(ScreenManager* screenManager)
	{
		if (screenManager == nullptr)
		{
			//SOMETHING WENT WRONG.
		}
		m_screenManager = screenManager;
	}
}