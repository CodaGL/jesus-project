#include "Mob.h"
#include "Level.h"
#include "Player.h"
#include "Math.h"
#include "globals.h"

void jet::Mob::processCollisions()
{
	m_collisionFlags = ECollisionFlags::None;

	//if (!m_isCollidableX && !m_isCollidableY)
		//return;

	const sf::Vector2f newPosition = m_level->handleMobCollisions(*this);//m_level->handleCollision({ m_x, m_y }, m_xr, m_yr, m_velocity.x, m_velocity.y, &m_collisionFlags);

	if(m_isCollidableX)
		m_x = newPosition.x;
	if(m_isCollidableY)
	{
		m_y = newPosition.y;
		if (m_collisionFlags & ECollisionFlags::Floor)
		{
			m_velocity.y = 0.0f;
		}
		else if (m_collisionFlags & ECollisionFlags::Ceil)
		{
			m_velocity.y = 0.123f;
		}
	}
	m_wasOnGround = m_onGround;
	m_onGround = (m_collisionFlags & ECollisionFlags::Floor);		
}

void jet::Mob::render(sf::RenderTarget& target) const
{
	jet::AnimatedEntity::render(target);
	
	if(m_maxHealth > 0)
	{
		/* Health Drawing*/
		float xOffset = 1.0f;
		float yOffset = 5.0f;
		float sizeX = 10.0f;
		float sizeY = 2.0f;

		sf::RectangleShape backgroundShape{ { sizeX, sizeY } };
		backgroundShape.setPosition(this->getLeft(), this->getTop() - yOffset);

		float percentage = (m_health / m_maxHealth);

		sf::RectangleShape frontShape{ { sizeX * percentage, sizeY } };
		frontShape.setPosition(this->getLeft(), this->getTop() - yOffset);
		frontShape.setFillColor(sf::Color::Red);

		target.draw(backgroundShape);
		target.draw(frontShape);
	}
}


void jet::Mob::tick()
{
	//
	if(m_scriptedBehavior != nullptr)
	{
		m_scriptedBehavior->tick();
	}

	m_velocity.x = Math::clamp(m_velocity.x, -kMaxAccelerationX, kMaxAccelerationX);
	setX(m_x + m_velocity.x);

	if(m_applyGravity)
	{
		m_velocity.y += kGravity;
		if (m_velocity.y > kMaxAccelerationY)
			m_velocity.y = kMaxAccelerationY;
	}

	setY(m_y + m_velocity.y);
	

	if(m_knockBackTicks > 0)
	{
		m_knockBackTicks--;
		setPosition(m_x + m_knockBackVelocity.x, m_y + m_knockBackVelocity.y);
	}
}

void jet::Mob::postTick()
{
	m_animatedSprite.setFlippedX(m_direction == LEFT);
}

bool jet::Mob::isGrounded() const
{
	return m_collisionFlags & ECollisionFlags::Floor;
}

bool jet::Mob::isCeiled() const
{
	return m_collisionFlags & ECollisionFlags::Ceil;
}

const sf::Vector2f& jet::Mob::getVelocity() const
{
	return m_velocity;
}

void jet::Mob::setVelocity(const sf::Vector2f& velocity)
{
	setVelocity(velocity.x, velocity.y);
}

void jet::Mob::setHealth(int health)
{
	if (health > m_maxHealth)
		health = m_maxHealth;
	m_health = health;
}

int jet::Mob::getHealth() const
{
	return m_health;
}

int jet::Mob::getMaxHealth() const
{
	return m_maxHealth;
}

void jet::Mob::damage(int health, Entity* damagedBy)
{
	m_health -= health;
	if(m_health <= 0)
	{
		m_health = 0;
		onDie(damagedBy);
	}
	jet::AnimatedEntity::setSpriteColor(sf::Color(244, 110, 66), sf::seconds(1.0f));
}

void jet::Mob::setVelocity(const float x, const float y)
{
	m_velocity.x = x;
	m_velocity.y = y;
}

jet::Direction jet::Mob::getDirection() const
{
	return m_direction;
}

void jet::Mob::loadAnimations(const std::string& name, float frameSizeX, float frameSizeY)
{
	sf::Texture& texture = *g_resourceManager->getTexture(name);

	const int idle_frames = 1;
	const int walking_frames = 6;
	const int in_air_frames = 1;
	const int jumping_frames = 2;
	const int on_ladder_idle_frames = 1;
	const int on_ladder_moving_frames = 4;
	const int attacking_frames = 4;

	int offset = 0;

	//Idle
	Animation* anim = new Animation();
	anim->setTexture(&texture);
	for (int i = 0; i < idle_frames; i++)
	{
		anim->addFrame(sf::IntRect(offset * frameSizeX, 0, frameSizeX, frameSizeY));
		offset++;
	}
	addAnimation(EntityState::Idle, anim);

	//Walk Anim
	anim = new Animation(sf::seconds(0.15f), true);
	anim->setTexture(&texture);
	for (int i = 0; i < walking_frames; ++i)
	{
		anim->addFrame(sf::IntRect(offset * frameSizeX, 0, frameSizeX, frameSizeY));
		offset++;
	}
	addAnimation(EntityState::Moving, anim);

	//Jump Anim
	anim = new Animation();
	anim->setTexture(&texture);
	for (int i = 0; i < jumping_frames; ++i)
	{
		anim->addFrame(sf::IntRect(offset * frameSizeX, 0, frameSizeX, frameSizeY));
		offset++;
	}
	addAnimation(EntityState::Jumping, anim);

	//In Air Anim
	anim = new Animation();
	anim->setTexture(&texture);
	for (int i = 0; i < in_air_frames; ++i)
	{
		anim->addFrame(sf::IntRect(offset * frameSizeX, 0, frameSizeX, frameSizeY));
		offset++;
	}
	addAnimation(EntityState::InAir, anim);

	/*OnLadder Idle Anim
	anim = new Animation();
	anim->setTexture(&texture);
	for (int i = 0; i < on_ladder_idle_frames; ++i)
	{
	anim->addFrame(sf::IntRect(offset * 32, 0, 32, 32));
	offset++;
	}
	addAnimation(EntityState::OnLadder_Idle, anim);*/

	//OnLadder Walking Anim
	anim = new Animation(sf::seconds(0.2f));
	anim->setTexture(&texture);
	for (int i = 0; i < on_ladder_moving_frames; ++i)
	{
		anim->addFrame(sf::IntRect(offset * frameSizeX, 0, frameSizeX, frameSizeY));
		offset++;
	}
	addAnimation(EntityState::OnLadder_Moving, anim);

	anim = new Animation(sf::seconds(0.1f), false);
	anim->setTexture(&texture);
	for (int i = 0; i < attacking_frames; ++i)
	{
		anim->addFrame(sf::IntRect(offset * frameSizeX, 0, frameSizeX, frameSizeY));
		offset++;
	}
	addAnimation(EntityState::Attacking, anim);
}

