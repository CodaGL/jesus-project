#include "LevelReader.h"
#include "LevelData.h"
#include <iostream>
#include "EnemyData.h"

#define XMLCheckResult(result) if (result != tinyxml2::XML_SUCCESS) { std::cout << "MapReader: XML file could not be read, error: " << std::to_string(static_cast<int>(result)); return false; }

bool LevelReader::readLevel(const std::string& path, LevelData& levelData)
{
	tinyxml2::XMLDocument document;
	tinyxml2::XMLError error = document.LoadFile(path.c_str());
	if(error != tinyxml2::XML_SUCCESS)
	{
		printf("Cannot open tmx file.");
		return false;
	}
	tinyxml2::XMLElement* map = document.FirstChildElement("map");
	if(map == nullptr)
	{
		printf("Unable to load map\n");
		return false;
	}
	if (!readLevelProperties(map, levelData))
	{
		std::cout << "Unable to load level properties! (loadLevelProperties failed!)" << std::endl;
		return false;
	}
	if(!readGlobalGids(map))
	{
		std::cout << "GID problems!" << std::endl;
		return false;
	}
	if (!readLevelLayers(map, levelData))
	{
		std::cout << "Unable to load level layers! (loadLevelLayers failed!)" << std::endl;
		return false;
	}
	if (!readObjects(map, levelData))
	{
		std::cout << "Unable to load objects! (loadObjects failed!)" << std::endl;
		return false;
	}
	return true;
}

bool LevelReader::readLevelProperties(tinyxml2::XMLElement* root, LevelData& levelData)
{
	std::cout << "Loading Level Properties!" << std::endl;
	levelData.width = root->IntAttribute("width");
	levelData.height = root->IntAttribute("height");

	//Custom Properties Loading
	{
		tinyxml2::XMLElement* properties = root->FirstChildElement("properties");
		tinyxml2::XMLElement* property = properties->FirstChildElement("property");
		while (property != nullptr)
		{
			levelData.m_properties[property->Attribute("name")] = property->Attribute("value");

			property = properties->NextSiblingElement("property");
		}
	}

	levelData.lightLevel = levelData.hasProperty("light_level") ? levelData.getProperty<float>("light_level") : 1.0f;
	
	std::cout << "Level Properties: " << std::endl;
	for(auto&& property : levelData.m_properties)
	{
		std::cout << property.first << ": " << property.second << std::endl;
	}

	std::cout << "Level Properties loaded successfully!" << std::endl;
	return true;
}

bool LevelReader::readGlobalGids(tinyxml2::XMLElement* root)
{
	tinyxml2::XMLElement* tileset = root->FirstChildElement("tileset");
	while(tileset != nullptr)
	{
		const char* text;
		auto result = tileset->QueryStringAttribute("name", &text);
		XMLCheckResult(result);

		std::string name = text;

		int gid;
		
		result = tileset->QueryIntAttribute("firstgid", &gid);
		XMLCheckResult(result);

		if(name.find("enemy") != std::string::npos || name.find("enemies") != std::string::npos)
		{
			m_gidEnemies = gid;
		}
		tileset = tileset->NextSiblingElement("tileset");
	}
	return true;
}

bool LevelReader::readLevelLayers(tinyxml2::XMLElement* root, LevelData& levelData)
{
	tinyxml2::XMLElement* layer = root->FirstChildElement("layer");
	while (layer != nullptr)
	{
		std::string name = layer->Attribute("name");
		tinyxml2::XMLElement* layerData = layer->FirstChildElement("data");

		if(layerData != nullptr)
		{
			const std::string data = layerData->GetText();
			if(name.find("collidables") != std::string::npos)
			{
				if (!readCollidableTileLayer(data, levelData))
					return false;
			}
			else if(name.find("background") != std::string::npos)
			{
				if (!readBackgroundTileLayer(data, levelData))
					return false;
			}
			else if (name.find("middleground") != std::string::npos)
			{
				if (!readMiddlegroundTileLayer(data, levelData))
					return false;
			}
			else if (name.find("foreground") != std::string::npos)
			{
				if (!readForegroundTileLayer(data, levelData))
					return false;
			}
			else if(name.find("ladders") != std::string::npos)
			{
				if (!loadLaddersTileLayer(data, levelData))
					return false;
			}
		}
		layer = layer->NextSiblingElement("layer");
	}
	return true;
}

bool LevelReader::readObjects(tinyxml2::XMLElement* root, LevelData& levelData)
{
	tinyxml2::XMLElement* objectGroup = root->FirstChildElement("objectgroup");
	while (objectGroup != nullptr)
	{
		std::string name = objectGroup->Attribute("name");

		std::cout << "Loading: " << name << std::endl;
		if(name.find("light") != std::string::npos)
		{
			if (!readLights(objectGroup, levelData))
				return false;
		}
		else if(name.find("enemy") != std::string::npos)
		{
			if (!readEnemies(objectGroup, levelData))
				return false;
		}

		objectGroup = objectGroup->NextSiblingElement("objectgroup");
	}
	return true;
}

bool LevelReader::readLights(tinyxml2::XMLElement* group, LevelData& levelData)
{
	tinyxml2::XMLElement* object = group->FirstChildElement("object");
	while (object != nullptr)
	{
		//float lightX = object->Attribute("x", );
		float lightX = object->FloatAttribute("x", 0);
		float lightY = object->FloatAttribute("y", 0);
		float width = object->FloatAttribute("width", 0);
		float height = object->FloatAttribute("height", 0);

		jet::LightData lightData;
		lightData.radius.x = width / 64; //64 is lightMap's size.
		lightData.radius.y = height / 64; //64 is lightMap's size.
		lightData.position.x = lightX + width/2.0f;
		lightData.position.y = lightY + height/2.0f;

		levelData.lights.push_back(lightData);
		object = object->NextSiblingElement("object");
	}
	return true;
}

bool LevelReader::readEnemies(tinyxml2::XMLElement* group, LevelData& levelData)
{
	tinyxml2::XMLElement* object = group->FirstChildElement("object");
	while (object != nullptr)
	{
		tinyxml2::XMLError result;
		
		//id not used yet. It's unique objects id.
		int id;
		result = object->QueryIntAttribute("id", &id);
		XMLCheckResult(result);

		int x;
		result = object->QueryIntAttribute("x", &x);
		XMLCheckResult(result);

		int y;
		result = object->QueryIntAttribute("y", &y);
		XMLCheckResult(result);

		int gid;
		result = object->QueryIntAttribute("gid", &gid);
		XMLCheckResult(result);

		jet::EnemyData enemyData;
		
		std::cout << (gid - m_gidEnemies + 1) << std::endl;
		std::cout << gid << ", " << m_gidEnemies << std::endl;
		enemyData.id = static_cast<jet::EnemyID>(gid - m_gidEnemies + 1);
		
		enemyData.objectId = id;
		enemyData.spawnPosition = sf::Vector2f(static_cast<float>(x), static_cast<float>(y));

		levelData.enemies.push_back(enemyData);
		std::cout << "Enemy read" << std::endl;
		object = object->NextSiblingElement("object");
	}
	return true;
}


bool LevelReader::readBackgroundTileLayer(const std::string& layer, LevelData& levelData)
{
	std::string modifyable = layer;
	size_t pos = 0;
	std::vector<jet::TileData> backgroundLayer;
	while( (pos = modifyable.find(',')) != std::string::npos)
	{
		const int v = std::stoi(modifyable.substr(0, pos));
		backgroundLayer.push_back(v);
		modifyable.erase(0, pos + 1);
	}
	backgroundLayer.push_back(std::stoi(modifyable));

	levelData.backgroundLayers.push_back(backgroundLayer);
	return true;
}

bool LevelReader::readCollidableTileLayer(const std::string& layer, LevelData& levelData)
{
	std::string modifyable = layer;
	size_t pos = 0;
	while ((pos = modifyable.find(',')) != std::string::npos)
	{
		if (std::stoi(modifyable.substr(0, pos)) > 0)
			levelData.collidableTiles.push_back(true);
		else
			levelData.collidableTiles.push_back(false);
		modifyable.erase(0, pos + 1);
	}
	levelData.collidableTiles.push_back(std::stoi(modifyable) > 0 ? true : false);

	return true;
}

bool LevelReader::readForegroundTileLayer(const std::string& layer, LevelData& levelData)
{
	std::string modifyable = layer;
	size_t pos = 0;
	std::vector<jet::TileData> foregroundLayer;
	while ((pos = modifyable.find(',')) != std::string::npos)
	{
		const int v = std::stoi(modifyable.substr(0, pos));
		foregroundLayer.push_back(v);
		modifyable.erase(0, pos + 1);
	}
	foregroundLayer.push_back(std::stoi(modifyable));

	levelData.foregroundLayers.push_back(foregroundLayer);
	return true;
}

bool LevelReader::readMiddlegroundTileLayer(const std::string& layer, LevelData& levelData)
{
	std::string modifyable = layer;
	size_t pos = 0;
	std::vector<jet::TileData> middlegroundLayer;
	while ((pos = modifyable.find(',')) != std::string::npos)
	{
		const int v = std::stoi(modifyable.substr(0, pos));
		middlegroundLayer.push_back(v);
		modifyable.erase(0, pos + 1);
	}
	middlegroundLayer.push_back(std::stoi(modifyable));

	levelData.middlegroundLayers.push_back(middlegroundLayer);
	return true;
}

bool LevelReader::loadLaddersTileLayer(const std::string& layer, LevelData& levelData)
{
	std::string modifyable = layer;
	size_t pos = 0;
	while ((pos = modifyable.find(',')) != std::string::npos)
	{
		if (std::stoi(modifyable.substr(0, pos)) > 0)
			levelData.laddersLayer.push_back(true);
		else
			levelData.laddersLayer.push_back(false);
		modifyable.erase(0, pos + 1);
	}
	levelData.laddersLayer.push_back(std::stoi(modifyable) > 0 ? true : false);
	return true;
}