#include "LevelEditorScreen.h"
#include "imgui-SFML.h"
#include <imgui.h>
#include "ResourceManager.h"
#include <imgui_internal.h>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Mouse.hpp>
#include "globals.h"
#include "InputManager.h"

namespace jet
{
	void LevelEditorScreen::onEnter()
	{
		//m_currentTileset.setTexture(g_resourceManager->getTexture("tileset_1"));

		view.setSize(1280.0f, 720.0f);
		view.setCenter(1280.0f / 2.0f, 720.0f / 2.0f);
		m_zoom = 1.0f;

		generateNewLevel(10, 10);
	}

	void LevelEditorScreen::onExit()
	{
	}

	void LevelEditorScreen::render(sf::RenderTarget& target)
	{
		target.setView(view);
		if(m_currentTileset.getTexture() != nullptr)
		{
			sf::RenderStates states;
			states.texture = m_currentTileset.getTexture();
			for(auto&& layer : m_vertexMap)
			{
				target.draw(layer, states);
			}
		}
		target.draw(tilesGrid);
		renderGUI(target);
	}

	void LevelEditorScreen::tick()
	{
		if (!ImGui::IsMouseHoveringAnyWindow())
		{
			if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
			{
				sf::Vector2i tileIndex = static_cast<sf::Vector2i>(g_inputManager->getMousePosition(view) / 32.0f);
				printf("%d - %d\n", tileIndex.x, tileIndex.y);
				if (tileIndex.x >= 0 && tileIndex.x < 100 && tileIndex.y >= 0 && tileIndex.y < 100)
				{
					//setTile(tileIndex.x, tileIndex.y, m_currentTileId);
					setTile(tileIndex.x, tileIndex.y, m_currentTileId);
				}
			}
		}
		if(!ImGui::IsAnyItemFocused())
		{
			static const int speed = 8;
			if (g_inputManager->isKeyPressed(InputManager::Key::MoveLeft))
			{
				view.move(-1.0f*speed, 0.0f);
			}
			else if (g_inputManager->isKeyPressed(InputManager::Key::MoveRight))
			{
				view.move(1.0f*speed, 0.0f);
			}
			if (g_inputManager->isKeyPressed(InputManager::Key::MoveUp))
			{
				view.move(0.0f, -1.0f*speed);
			}
			else if (g_inputManager->isKeyPressed(InputManager::Key::MoveDown))
			{
				view.move(0.0f, 1.0f*speed);
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
			{
				m_zoom *= 1.1f;
				view.zoom(1.1f);
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::E))
			{
				m_zoom *= 0.9f;
				view.zoom(0.9f);
			}
		}
	}

	void LevelEditorScreen::renderGUI(sf::RenderTarget& target)
	{
		//Workaround
		bool newLevelPopup = false;
		//
		//ImGui::ShowDemoWindow();
		ImGui::Begin("Level Editor", 0, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoMove);
		ImGui::SetWindowSize(sf::Vector2f(500, 600));
		ImGui::SetWindowPos(sf::Vector2f(1280.0f - 510, 720.0f / 2.0f - 300));
		if (ImGui::BeginMenuBar())
		{
			if (ImGui::BeginMenu("File"))
			{
				if (ImGui::MenuItem("New Level"))
				{
					newLevelPopup = true;
				}
				if (ImGui::MenuItem("Load Level")) {}
				if (ImGui::MenuItem("Close Level")) {}
				ImGui::EndMenu();
			}
			ImGui::EndMenuBar();
			if (ImGui::CollapsingHeader("Tilesets", 0, true, false))
			{
				const char* items[] = { "tileset_1" };
				static const char* currentItem = 0;
				if (ImGui::BeginCombo("SpriteSheet/TileSet", currentItem)) // The second parameter is the label previewed before opening the combo.
				{
					for (int n = 0; n < IM_ARRAYSIZE(items); n++)
					{
						bool is_selected = (currentItem == items[n]); // You can store your selection however you want, outside or inside your objects
						if (ImGui::Selectable(items[n], is_selected))
						{
							currentItem = items[n];
							m_currentTileset.setTexture(*g_resourceManager->getTexture(items[n]));
						}
						if (is_selected)
							ImGui::SetItemDefaultFocus();   // Set the initial focus when opening the combo (scrolling + for keyboard navigation support in the upcoming navigation branch)
					}
					ImGui::EndCombo();
				}
				static int spacingX = 0, spacingY = 0;
				ImGui::SliderInt("Spacing X", &spacingX, 0, 32);
				ImGui::SliderInt("Spacing Y", &spacingY, 0, 32);
				ImGui::SliderInt("Current Layer", &m_currentLayer, 0, m_layers.size()-1);
				if(ImGui::Button("Add Layer"))
				{
					addLayer();
				}
				//ImGui::List

				if (ImGui::TreeNode("Grid"))
				{
					if(m_currentTileset.getTexture() != nullptr)
					{
						const int tilesWidth = m_currentTileset.getTexture()->getSize().x / 32.0f;
						const int tilesHeight = m_currentTileset.getTexture()->getSize().y / 32.0f;
						for (int y = 0; y < tilesHeight; ++y)
						{
							//ImGui::BeginColumns("tilescolumns", tilesWidth, ImGuiColumnsFlags_NoBorder);
							for (int x = 0; x < tilesWidth; ++x)
							{
								ImGui::PushID(x + y * tilesWidth);
								m_currentTileset.setTextureRect(sf::IntRect(x * 32, y * 32, 32, 32));
								if (ImGui::ImageButton(m_currentTileset, sf::Vector2f(16, 16)))
								{
									m_currentTileId = x + y * tilesWidth;
								}
								ImGui::PopID();
								if (x < tilesWidth)
									ImGui::SameLine();
							}
							ImGui::NewLine();
						}
					}
					else
					{
						ImGui::Text("No Tilesheet selected!");
					}
					ImGui::TreePop();
				}
				char buf[32];
				sprintf_s(buf, "Current Tile Id: %d", m_currentTileId);
				
				ImGui::Text(buf);
			}


		}
		if(newLevelPopup)
		{
			ImGui::OpenPopup("New Level Popup");
		}
		if (ImGui::BeginPopupModal("New Level Popup"))
		{
			static char levelName[32];
			static int levelWidth = 0, levelHeight = 0;
			ImGui::InputText("Level Name", levelName, 32);
			ImGui::InputInt("Level Width", &levelWidth);
			ImGui::InputInt("Level Height", &levelHeight);
			if(ImGui::Button("Create Level"))
			{
				if(levelWidth > 0 && levelHeight > 0)
				{
					generateNewLevel(levelWidth, levelHeight);
					ImGui::CloseCurrentPopup();
				}
			}
			ImGui::EndPopup();
		}
		ImGui::End();
	}

	void LevelEditorScreen::generateLayerMesh(int layeri)
	{
		if (m_currentTileset.getTexture() == nullptr)
			return;
		sf::VertexArray& vertexLayer = m_vertexMap[layeri];
		
		vertexLayer.clear();
		vertexLayer.setPrimitiveType(sf::PrimitiveType::Quads);
		vertexLayer.resize(m_levelWidth * m_levelHeight * 4);

		const int tilesWidth = m_currentTileset.getTexture()->getSize().x / 32.0f;
		const int tilesHeight = m_currentTileset.getTexture()->getSize().y / 32.0f;
		const int tileSize = 32;
		for (int y = 0; y < m_levelHeight; ++y)
		{
			for (int x = 0; x < m_levelWidth; ++x)
			{
				const TileData tileData = m_layers[layeri][x + y * m_levelWidth];
				if (tileData == 0)
					continue;
				const int tu = tileData % tilesWidth;
				const int tv = tileData / tilesWidth;

				sf::Vertex* quad = &vertexLayer[(x + y * m_levelWidth) * 4];

				quad[0].position = sf::Vector2f(x * tileSize, y * tileSize);
				quad[1].position = sf::Vector2f((x + 1) * tileSize, y * tileSize);
				quad[2].position = sf::Vector2f((x + 1) * tileSize, (y + 1) * tileSize);
				quad[3].position = sf::Vector2f(x * tileSize, (y + 1) * tileSize);

				quad[0].texCoords = sf::Vector2f(tu * tileSize, tv * tileSize);
				quad[1].texCoords = sf::Vector2f((tu + 1) * tileSize, tv * tileSize);
				quad[2].texCoords = sf::Vector2f((tu + 1) * tileSize, (tv + 1) * tileSize);
				quad[3].texCoords = sf::Vector2f(tu * tileSize, (tv + 1) * tileSize);
			}
		}
	}

	void LevelEditorScreen::addLayer()
	{
		m_layers.emplace_back();
		for(int i = 0; i < m_levelWidth*m_levelHeight; ++i)
		{
			m_layers[m_layers.size() - 1].push_back(0);
		}

		m_vertexMap.emplace_back();
		generateLayerMesh(m_vertexMap.size() - 1);
	}

	void LevelEditorScreen::setTile(int x, int y, TileData id, bool rebuild)
	{
		if (x < 0 || x >= m_levelWidth || y < 0 || y >= m_levelHeight || m_currentTileset.getTexture() == nullptr)
			return;
		m_layers[m_currentLayer][x + y * m_levelWidth] = m_currentTileId;
		if(rebuild)
			generateLayerMesh(m_currentLayer);
	}

	void LevelEditorScreen::generateGridMesh(const int mapWidth, const int mapHeight)
	{

		tilesGrid.clear();
		tilesGrid.setPrimitiveType(sf::PrimitiveType::Lines);
		tilesGrid.resize(m_levelWidth * m_levelHeight * 2);

		const int tileSize = 32;
		for (int x = 0; x < m_levelWidth; ++x)
		{
			sf::Vertex* line = &tilesGrid[(x + 0 * m_levelWidth) * 2];
			printf("%d - ", (x + 0 * m_levelWidth) * 2);
			line[0].position = sf::Vector2f(x * tileSize, 0 * tileSize);
			line[1].position = sf::Vector2f(x * tileSize, m_levelHeight * tileSize);

			line[0].color = sf::Color::Black;
			line[1].color = sf::Color::Black;
		}
		printf("\n");
		for (int y = 0; y < m_levelHeight; ++y)
		{
			sf::Vertex* line = &tilesGrid[(0 + y * m_levelWidth) * 2];
			printf("%d - ", (0 + y * m_levelWidth) * 2);
			line[0].position = sf::Vector2f(0 * tileSize, y * tileSize);
			line[1].position = sf::Vector2f(m_levelWidth * tileSize, y * tileSize);

			line[0].color = sf::Color::Black;
			line[1].color = sf::Color::Black;
		}
	}

	void LevelEditorScreen::generateNewLevel(int levelWidth, int levelHeight)
	{
		m_levelWidth = levelWidth;
		m_levelHeight = levelHeight;

		addLayer();

		generateGridMesh(levelWidth, levelHeight);
	}
}
