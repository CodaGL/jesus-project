#pragma once
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RenderTexture.hpp>
#include "TileMap.h"
#include "EntityType.h"

struct LevelData;

namespace jet
{
	class LevelScreen;
	class Level;

	class LevelRenderer
	{
	public:
		LevelRenderer(jet::LevelScreen* levelScreen, Level& level, LevelData& levelData);
		~LevelRenderer();

		void renderBackground(sf::RenderTarget& target, sf::RenderStates states);
		void renderMiddleground(sf::RenderTarget& target, sf::RenderStates states);
		void renderForeground(sf::RenderTarget& target, sf::RenderStates states);

		void renderObjects(jet::EntityType entityType, sf::RenderTarget& target, sf::RenderStates states);

		void renderLights(sf::RenderTarget& target);

		void resize(int width, int height);

		jet::Level& getLevel() const;

	protected:
		jet::Level& m_level;
		LevelData& m_levelData;

		jet::LevelScreen* m_levelScreen;

	private:

		jet::TileMap m_backgroundTileMap;
		jet::TileMap m_middlegroundTileMap;
		jet::TileMap m_foregroundTileMap;

		sf::RenderTexture m_lightMapTexture;
		sf::Sprite m_lightMap;
		sf::Sprite m_light;

		void generateTileMap();
		void generateLightMap();

	};
}
