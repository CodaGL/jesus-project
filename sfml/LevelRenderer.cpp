#include "LevelRenderer.h"
#include "globals.h"
#include "ResourceManager.h"
#include "LevelRenderer.h"
#include "TileMap.h"
#include "Level.h"
#include "LevelData.h"
#include "Player.h"
#include "LevelScreen.h"

inline bool fastIntersects(const sf::FloatRect& b1, const sf::FloatRect& b2)
{
	return !(b2.left >= b1.left + b1.width
		|| b1.left >= b2.left + b2.width
		|| b2.top >= b1.top + b1.height
		|| b1.top >= b2.top + b2.height);
}
inline bool isInsideView(const sf::View& view, const sf::FloatRect& rect)
{
	sf::FloatRect viewRect
	{
		view.getCenter().x - view.getSize().x / 2.0f,
		view.getCenter().y - view.getSize().y / 2.0f,
		view.getSize().x,
		view.getSize().y,
	};
	viewRect.left -= 20.0f;
	viewRect.top -= 20.0f;
	viewRect.width += 2*20.0f;
	viewRect.height += 2*20.0f;
	return fastIntersects(viewRect, rect);
	//return fastIntersects();
}

jet::LevelRenderer::LevelRenderer(jet::LevelScreen* levelScreen, Level& level, LevelData& levelData) :
	m_level(level),
	m_levelData(levelData),
	m_levelScreen(levelScreen)
{
	generateTileMap();
	generateLightMap();
}

jet::LevelRenderer::~LevelRenderer()
{
}

void jet::LevelRenderer::resize(int width, int height)
{
	/*m_lightMapTexture.create(width, height);
	m_lightMap.setTexture(m_lightMapTexture.getTexture());*/
}

jet::Level& jet::LevelRenderer::getLevel() const
{
	return m_level;
}

void jet::LevelRenderer::renderBackground(sf::RenderTarget& target, sf::RenderStates states)
{
	m_backgroundTileMap.draw(target, states);
}

void jet::LevelRenderer::renderMiddleground(sf::RenderTarget& target, sf::RenderStates states)
{
	m_middlegroundTileMap.draw(target, states);
}

void jet::LevelRenderer::renderForeground(sf::RenderTarget& target, sf::RenderStates states)
{
	m_foregroundTileMap.draw(target, states);
}

void jet::LevelRenderer::renderObjects(jet::EntityType entityType, sf::RenderTarget& target, sf::RenderStates states)
{
	if(entityType == _Player)
	{
		m_level.getPlayer().render(target);
		return;
	}
	const sf::View view = target.getView();
	for (auto&& entity : m_level.getEntitiesOfType(entityType))
	{
		if (isInsideView(view, { entity->getLeft(), entity->getTop(), 32.0f, 32.0f }))
		{
			entity->render(target);
		}
	}
	/*const int minTx = static_cast<int>((view.getCenter().x - view.getSize().x / 2.0f) / 32.0f - 1);
	const int maxTx = static_cast<int>((view.getCenter().x + view.getSize().x / 2.0f) / 32.0f + 1);
	const int minTy = static_cast<int>((view.getCenter().y - view.getSize().y / 2.0f) / 32.0f - 1);
	const int maxTy = static_cast<int>((view.getCenter().y + view.getSize().y / 2.0f) / 32.0f + 1);
	for (int y = minTy; y <= maxTy; ++y)
	{
		if (y < 0 || y >= m_levelData.height)
			continue;
		for (int x = minTx; x <= maxTx; ++x)
		{
			if (x < 0 || x >= m_levelData.width)
				continue;
			auto&& entitiesToDraw = m_level.getEntitiesInTile(x, y);
			for (auto&& entity : entitiesToDraw)
			{
				entity->render(target);
			}
		}
	}

	m_level.getPlayer().render(target);

	for (int y = minTy; y <= maxTy; ++y)
	{
		if (y < 0 || y >= m_levelData.height)
			continue;
		for (int x = minTx; x <= maxTx; ++x)
		{
			if (x < 0 || x >= m_levelData.width)
				continue;
			auto&& entitiesToDraw = m_level.getEntitiesInTile(x, y);
			for (auto&& entity : entitiesToDraw)
			{
				entity->renderDebug(target);
			}
		}
	}

	m_level.getPlayer().renderDebug(target);*/
}

void jet::LevelRenderer::renderLights(sf::RenderTarget& target)
{
	//m_lightMapTexture.clear({ 50, 50, 80 });
	m_lightMapTexture.clear({ static_cast<sf::Uint8>(255 * m_levelData.lightLevel),  static_cast<sf::Uint8>(255 * m_levelData.lightLevel), static_cast<sf::Uint8>(255 * m_levelData.lightLevel) });
	//renderObjects(jet::EntityType::_Light, target, sf::RenderStates::Default);
	for(auto&& light : m_level.getEntitiesOfType(_Light))
	{
		light->render(target);
	}
	m_lightMapTexture.display();

	m_lightMap.setTextureRect({ 0, 0, static_cast<int>(m_lightMapTexture.getSize().x), static_cast<int>(m_lightMapTexture.getSize().y) });
	m_lightMap.setPosition(0, 0);
	sf::RenderStates states;
	states.blendMode = sf::BlendMultiply;
	target.draw(m_lightMap, states);
}

void jet::LevelRenderer::generateTileMap()
{

	const int width = m_levelData.width;
	const int height = m_levelData.height;

	m_backgroundTileMap.load("tileset_1", width, height, m_levelData.backgroundLayers);
	m_middlegroundTileMap.load("tileset_1", width, height, m_levelData.middlegroundLayers);
	m_foregroundTileMap.load("tileset_1", width, height, m_levelData.foregroundLayers);
}


void jet::LevelRenderer::generateLightMap()
{
	m_lightMapTexture.create(m_levelData.width * 32, m_levelData.height * 32);
	m_lightMap.setTexture(m_lightMapTexture.getTexture());

	sf::Vector2u sz = g_resourceManager->getTexture("lightMap")->getSize();
	m_light.setTexture(*g_resourceManager->getTexture("lightMap"));
	//m_light.setTextureRect(sf::IntRect{ 0, 0, 32, 32 }); //OPTIONAL
	m_light.setOrigin(sz.x/2.0f, sz.y/2.0f);
}