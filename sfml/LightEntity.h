#pragma once
#include "Entity.h"
#include "LightData.h"
#include "EntityType.h"

namespace jet
{
	class LightEntity : public virtual Entity
	{
		
	public:
		LightEntity(LightData& data);

		void init() override;

		void update(const sf::Time& time) override;
		void render(sf::RenderTarget& target) const override;

		LightData& getLightData();

		jet::EntityType getEntityType() const override;

	private:
		sf::Sprite m_sprite;

		LightData m_lightData;

		float m_animationTime;

	};

	const float AMPLITUDE = 0.5f;
	const float FREQUENCY = 2.0f;
}
