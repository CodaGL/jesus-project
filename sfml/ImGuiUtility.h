#pragma once
#include <imgui.h>

namespace ImGui
{
	inline bool IsImGuiUsed()
	{
		return ImGui::IsAnyWindowHovered() || ImGui::IsAnyWindowFocused() || ImGui::IsAnyItemFocused() || ImGui::IsAnyItemHovered();
	}
}
