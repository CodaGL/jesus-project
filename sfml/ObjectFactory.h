#pragma once
#include "Singleton.h"
#include "Enemy.h"


#define REGISTER_ENEMY(ID, TYPE) \
	static jet::UtilityRegister regist(ID, \
	[]() -> jet::Enemy* { return new TYPE(); });

namespace jet
{

	typedef std::function<jet::Enemy*()> EnemyConstructor;

	class ObjectFactory final : public Singleton<ObjectFactory>
	{
	public:
		static jet::Enemy* createEnemy(jet::EnemyID enemy_id);
		static void registerEnemy(jet::EnemyID enemy_id, EnemyConstructor constructor);
	
	private:
		std::unordered_map<jet::EnemyID, EnemyConstructor> m_enemies;
	};

	class UtilityRegister final
	{
	public:
		UtilityRegister(jet::EnemyID eid, EnemyConstructor constructor);
	};
}
