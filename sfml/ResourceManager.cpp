#include "ResourceManager.h"
#include <iostream>

jet::ResourceManager* g_resourceManager;

jet::ResourceManager::ResourceManager()
{
	init();
}


jet::ResourceManager::~ResourceManager()
{
	std::cout << "Resource Manager Destructor called..." << std::endl;
	for (auto&& element : m_textures)
	{
		delete element.second;
	}
	for(auto&& element : m_bitmapFonts)
	{
		delete element.second;
	}
}

void jet::ResourceManager::init()
{
	loadTexture("tileset_1", "res/levels/index_map/tileset_index_level.png");
	loadTexture("jesus", "res/textures/mighel/jesus.png");
	loadTexture("jesus_spritesheet", "res/textures/mighel/jesus_spritesheet.png");
	loadTexture("skeleton_spritesheet", "res/textures/enemies/skeleton_spritesheet.png");
	loadTexture("skeleton_red_eyes_spritesheet", "res/textures/enemies/skeleton_red_eyes_spritesheet.png");
	loadTexture("magic_wand_spritesheet", "res/textures/weapons/magic_wand_spritesheet.png");
	loadTexture("heart", "res/textures/ui/heart.png");
	loadTexture("lightMap", "res/textures/particles/lightmap.png");
	loadTexture("ninepatch_1", "res/textures/ui/ninepatch_1.png");
	loadBitmapFont("default_12", "res/textures/fonts/default_12px.png");
	
	//Load weapon Textures
	loadTexture("right_hand_sword", "res/textures/weapons/sword/right_hand_sword.png");
	//Load Weapon Items
	loadWeapon("iron_sword", "res/data/weapons/sword.json");

	for(auto&& texture : m_textures)
	{
		texture.second->setSmooth(false);
	}
}

sf::Texture* jet::ResourceManager::getTexture(const std::string& name)
{
	const auto& it = m_textures.find(name);
	if (it == m_textures.end())
		return nullptr;
	return m_textures[name];
}

jet::BitmapFont* jet::ResourceManager::getBitmapFont(const std::string& name)
{
	const auto& it = m_bitmapFonts.find(name);
	if (it == m_bitmapFonts.end())
		return nullptr;
	return m_bitmapFonts[name];
}

jet::ItemData* jet::ResourceManager::getWeapon(const std::string& name)
{
	const auto& it = m_weapons.find(name);
	if (it == m_weapons.end())
		return nullptr;
	return m_weapons[name];
}

void jet::ResourceManager::loadTexture(const std::string& name, const std::string& path)
{
	loadResource<sf::Texture>(m_textures, path, name);
}

void jet::ResourceManager::loadBitmapFont(const std::string& name, const std::string& path)
{
	loadResource<BitmapFont>(m_bitmapFonts, path, name);
}

void jet::ResourceManager::loadWeapon(const std::string& name, const std::string& path)
{
	loadResource<ItemData>(m_weapons, path, name);
}

template <typename T>
bool jet::ResourceManager::loadResource(std::map<std::string, T*>& container, const std::string& path, const std::string& key)
{
	if (path.empty() || container.find(key) != container.end())
		return false;

	T* resource = new T();
	if(!resource->loadFromFile(path))
	{
		return false;
	}
	container[key] = resource;
	return true;
}