#include "SkeletonBoss.h"
#include "globals.h"
#include "ResourceManager.h"
#include "LineRectangle.h"
#include "Player.h"
#include "ObjectFactory.h"
#include "EnemyID.h"

REGISTER_ENEMY(jet::EnemyID::Skeleton_Boss, jet::SkeletonBoss);

jet::SkeletonBoss::SkeletonBoss()
{
	m_xr = 6.0f;
	m_yr = 16.0f;
	m_health = m_maxHealth = 20;
	m_applyGravity = true;
	m_behaviorTicks = 0;
	m_scriptedBehavior = new ScriptedBehavior("res/scripts/test.nut", this);
	
	loadAnimations("skeleton_red_eyes_spritesheet", 32.0f, 32.0f);
	m_animatedSprite.setOrigin(32.0f / 2.0f, 32.0f / 2.0f);

	setEntityState(EntityState::Idle);
}

void jet::SkeletonBoss::tick()
{
	const EntityState _prevState = getEntityState();
	EntityState _newState = _prevState;


	Mob::tick();

	if (m_onGround)
	{
		if (!m_wasOnGround)
		{
			//m_acceleration.x = -(1.0f / 128.0f);
			m_velocity.x = -kMaxAccelerationX;
		}
		if (m_velocity.x < 0.0f && m_collisionFlags & ECollisionFlags::Left)
		{
			//m_acceleration.x = (1.0f / 128.0f);
			m_velocity.x = kMaxAccelerationX;
		}
		else if (m_velocity.x > 0.0f && m_collisionFlags & ECollisionFlags::Right)
		{
			//m_acceleration.x = -(1.0f / 128.0f);
			m_velocity.x = -kMaxAccelerationX;
		}
		//m_velocity.x += m_acceleration.x;
		m_applyGravity = false;
	}
	else
	{
		m_applyGravity = true;
	}

	if (abs(m_velocity.x) > 0.0f)
	{
		if (m_velocity.x < 0.0f)
		{
			m_direction = LEFT;
		}
		else if (m_velocity.x > 0.0f)
		{
			m_direction = RIGHT;
		}
		_newState = EntityState::Moving;
	}
	else
	{
		_newState = EntityState::Idle;
	}

	if (_newState != _prevState)
	{
		setEntityState(_newState, true);
	}
}

void jet::SkeletonBoss::update(const sf::Time& time)
{
	jet::AnimatedEntity::update(time);
}


void jet::SkeletonBoss::render(sf::RenderTarget& target) const
{
	jet::Mob::render(target);

	Utility::LineRectangle rectangleSize{ sf::Vector2f(m_x - m_xr, m_y - m_yr), sf::Vector2f(m_xr * 2, m_yr * 2) };

	//Change colors based on collisions
	if (m_collisionFlags & ECollisionFlags::Floor)
	{
		rectangleSize.setSideColor(Utility::LineRectangle::RectangleSide::Bot, sf::Color::Red);
	}

	if (m_collisionFlags & ECollisionFlags::Ceil)
	{
		rectangleSize.setSideColor(Utility::LineRectangle::RectangleSide::Top, sf::Color::Red);
	}

	if (m_collisionFlags & ECollisionFlags::Left)
	{
		rectangleSize.setSideColor(Utility::LineRectangle::RectangleSide::Left, sf::Color::Red);
	}

	if (m_collisionFlags & ECollisionFlags::Right)
	{
		rectangleSize.setSideColor(Utility::LineRectangle::RectangleSide::Right, sf::Color::Red);
	}

	target.draw(rectangleSize);
}

void jet::SkeletonBoss::onCollision(Entity* entity, ECollisionFlags collisionFlag)
{
	//Bad BAD BAD
	auto mob = dynamic_cast<Player*>(entity);
	if (mob != nullptr)
	{
		mob->damage(1, this);
		mob->setKnockback((m_velocity.x > 0.0f ? 1.0f : -1.0f) * 1.5f, -1.5f, 30);
	}
	m_acceleration.x = -m_acceleration.x;
	m_velocity.x = -m_velocity.x;
}
