#include "Entity.h"
#include "Level.h"
#include "LevelScreen.h"

void jet::Entity::renderDebug(sf::RenderTarget& target) const
{

}

void jet::Entity::render(sf::RenderTarget& target) const
{
	Utility::LineRectangle rectangleSize{ sf::Vector2f(m_x - m_xr, m_y - m_yr), sf::Vector2f(m_xr * 2, m_yr * 2) };

	//Change colors based on collisions
	if (m_collisionFlags & ECollisionFlags::Floor)
	{
		rectangleSize.setSideColor(Utility::LineRectangle::RectangleSide::Bot, sf::Color::Red);
	}

	if (m_collisionFlags & ECollisionFlags::Ceil)
	{
		rectangleSize.setSideColor(Utility::LineRectangle::RectangleSide::Top, sf::Color::Red);
	}

	if (m_collisionFlags & ECollisionFlags::Left)
	{
		rectangleSize.setSideColor(Utility::LineRectangle::RectangleSide::Left, sf::Color::Red);
	}

	if (m_collisionFlags & ECollisionFlags::Right)
	{
		rectangleSize.setSideColor(Utility::LineRectangle::RectangleSide::Right, sf::Color::Red);
	}

	target.draw(rectangleSize);
}

jet::EntityState jet::Entity::getEntityState() const
{
	return m_entityState;
}

void jet::Entity::setEntityState(jet::EntityState state)
{
	m_entityState = state;
}

const float jet::Entity::getX() const
{
	return m_x;
}

void jet::Entity::setX(float _x)
{
	setPosition(_x, m_y);
}

const float jet::Entity::getY() const
{
	return m_y;
}

void jet::Entity::setY(float _y)
{
	setPosition(m_x, _y);
}

const float jet::Entity::getLeft() const
{
	return m_x - m_xr;
}

const float jet::Entity::getRight() const
{
	return m_x + m_xr;
}

const float jet::Entity::getCenterX() const
{
	return m_x;
}

const float jet::Entity::getCenterY() const
{
	return m_y;
}

const float jet::Entity::getBottom() const
{
	return m_y + m_yr;
}

const float jet::Entity::getTop() const
{
	return m_y - m_yr;
}

const float jet::Entity::getXR() const
{
	return m_xr;
}

void jet::Entity::setXR(float _xr)
{
	m_xr = _xr;
}

const float jet::Entity::getYR() const
{
	return m_yr;
}

void jet::Entity::setYR(float _yr)
{
	m_yr = _yr;
}


void jet::Entity::setLevel(Level* level)
{
	m_level = level;
}

jet::Level* jet::Entity::getLevel()
{
	return m_level;
}

bool jet::Entity::contains(float x, float y, float sz_x, float sz_y) const
{
	return (getLeft() <= (x + sz_x) && getRight() >= x - sz_x 
	&& getTop() <= y + sz_y && getBottom() >= y - sz_y);
	//(m_x + m_xr >= x-sz_x && m_x-m_xr <= x + sz_x && m_y + m_yr >= y-sz_y && m_y-m_yr <= y + sz_y);

}
