#pragma once
#include <SFML/Graphics/Drawable.hpp>
#include "BitmapText.h"
#include <SFML/System/Clock.hpp>

namespace sf {
	class View;
}

namespace jet
{
	class DebugRenderer : public virtual sf::Drawable
	{

	public:
		DebugRenderer();

		void updateFPS();
		void updateTicks();

		void setView(sf::View& view);
		
	private:
		sf::View* m_view;

		sf::Clock m_ticksClock;
		int m_ticks;

		sf::Clock m_fpsClock;
		int m_frameCount;
		int m_fps;

		BitmapText m_tickText;
		BitmapText m_fpsText;

	private:
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	};
}
