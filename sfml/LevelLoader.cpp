#include "LevelLoader.h"
#include "ObjectFactory.h"
#include "Level.h"
#include "LevelData.h"
#include "LightEntity.h"

void jet::LevelLoader::loadObjects(LevelData& levelData, Level& level)
{
	loadLevelEnemies(levelData, level);
	loadLights(levelData, level);
}


void jet::LevelLoader::loadLevelEnemies(LevelData& levelData, Level& level)
{
	for (auto&& enemyData : levelData.enemies)
	{
		jet::Enemy* enemy = ObjectFactory::createEnemy(enemyData.id);
		enemy->setX(enemyData.spawnPosition.x);
		enemy->setY(enemyData.spawnPosition.y);
		level.add(enemy);
	}
}

void jet::LevelLoader::loadLights(LevelData& levelData, Level& level)
{
	for(auto&& light : levelData.lights)
	{
		jet::LightEntity* l = new jet::LightEntity(light);
		level.add(l);
	}
}
