#pragma once
#include "Mob.h"
#include "Enemy.h"

namespace jet
{
	class Skeleton : public Enemy
	{
	public:
		Skeleton();

		
		virtual void update(const sf::Time& time) override;

		void tick() override;

		void render(sf::RenderTarget& target) const override;

		void onCollision(Entity* entity, ECollisionFlags collisionFlag) override;

	protected:
		sf::Vector2f m_acceleration;

	private:

		int m_behaviorTicks;
	};
}
