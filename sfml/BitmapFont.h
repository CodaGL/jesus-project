#pragma once
#include <SFML/Graphics/Texture.hpp>

namespace jet
{
	class BitmapFont final
	{
	public:

		bool loadFromFile(const std::string& path);

		sf::Texture* getTexture();

	private:
		sf::Texture m_texture;
	};
}
