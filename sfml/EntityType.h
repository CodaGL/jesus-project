#pragma once

namespace jet
{
	enum EntityType
	{
		_Unknown,
		_Player,
		_Equipment, //Player Equipment
		_Enemy,
		_Light,
		_Damage,
		_Max
	};
}