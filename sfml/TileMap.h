#pragma once
#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include "Game.h"

namespace jet
{
	typedef uint16_t TileData;
	
	class TileMap : public sf::Drawable, public sf::Transformable
	{

	public:
		/*struct TileData
		{
			uint8_t id;
		};*/


	public:

		const uint8_t tileSize = 32;

		TileMap() = default;

		bool load(const std::string& tileSet, int width, int height, const std::vector<std::vector<TileData>>& layers);

		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;


	private:

		uint16_t m_width, m_height;

		std::vector<sf::VertexArray> m_layers;

		sf::Texture* m_texture;
	};
}