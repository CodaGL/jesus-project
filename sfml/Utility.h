#pragma once
#include <string>

inline void toUpperCase(std::string& toConvert)
{
	for (auto & c : toConvert) c = toupper(c);
}

inline void toLowerCase(std::string& toConvert)
{
	for (auto & c : toConvert) c = tolower(c);
}

