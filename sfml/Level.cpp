#include "Level.h"
#include "Player.h"
#include "Skeleton.h"
#include "globals.h"
#include "InputManager.h"
#include "DamageEntity.h"

jet::Level::Level(LevelData& data, jet::Player& player)
	: m_levelData(data),
	  m_player(player)
{
	for(int i = jet::EntityType::_Unknown; i < jet::EntityType::_Max; ++i)
	{
		m_entities.emplace_back();
	}
	m_entitiesInTile.reserve(data.width * data.height);
	for(int i = 0; i < data.width * data.height; ++i)
	{
		m_entitiesInTile.emplace_back();
	}
}

jet::Level::~Level()
{
	for (int i = jet::EntityType::_Unknown; i < jet::EntityType::_Max; ++i)
	{
		for (auto&& entity : m_entities[i])
		{
			delete entity;
		}
	}
}

void jet::Level::update(const sf::Time& time)
{
	m_player.update(time);
	for (int i = jet::EntityType::_Unknown; i < jet::EntityType::_Max; ++i)
	{
		for (auto&& entity : m_entities[i])
		{
			entity->update(time);
		}
	}
}


void jet::Level::tick()
{
	m_player.handleInput();

	m_player.tick();

	for (int ei = jet::EntityType::_Unknown; ei < jet::EntityType::_Max; ++ei)
	{
		for(size_t i = 0; i < m_entities[ei].size(); ++i)
		{
			jet::Entity& entity = *m_entities[ei][i];
			const int oldTileX = static_cast<int>(entity.getX() / 32.0f);
			const int oldTileY = static_cast<int>(entity.getY() / 32.0f);

			entity.tick();

			const int newTileX = static_cast<int>(entity.getX() / 32.0f);
			const int newTileY = static_cast<int>(entity.getY() / 32.0f);

			if (oldTileX != newTileX || oldTileY != newTileY)
			{
				removeEntity(oldTileX, oldTileY, &entity);
				insertEntity(newTileX, newTileY, &entity);
			}
		}
	}

	for (EntityType t = _Unknown; t < _Max; t = EntityType(t + 1)) {
		for (auto it = m_entities[t].begin(); it != m_entities[t].end(); /*don't increment here*/) {
			if (!(*it)->isAlive()) {
				removeEntity((*it)->getX() / 32.0f, (*it)->getY() / 32.0f, (*it));
				delete (*it);
				it = m_entities[t].erase(it);
			}
			else {
				++it;
			}
		}
	}
}

void jet::Level::postTick()
{
	m_player.postTick();
	for (int i = jet::EntityType::_Unknown; i < jet::EntityType::_Max; ++i)
	{
		for (auto&& entity : m_entities[i])
		{
			entity->postTick();
		}
	}
}

void jet::Level::processCollisions()
{
	m_player.processCollisions();
	for (int i = jet::EntityType::_Unknown; i < jet::EntityType::_Max; ++i)
	{
		for (auto&& entity : m_entities[i])
		{
			entity->processCollisions();
		}
	}
}

/*
void jet::Level::updateObjects(jet::EntityType et)
{
	for(auto&& e : m_entities[et])
	{
		e->tick();
	}
}
*/


jet::TileData jet::Level::getTile(int x, int y) const
{
	if (x < 0 || y < 0 || x >= m_levelData.width || y >= m_levelData.height)
		return 1;
	return m_levelData.collidableTiles[x + y * m_levelData.width] ? 1 : 0;
}

bool jet::Level::isCollidableAt(int x, int y) const
{
	if (x < 0 || y < 0 || x >= m_levelData.width || y >= m_levelData.height)
		return false;
	return m_levelData.collidableTiles[x + y * m_levelData.width];
}

bool jet::Level::isLadder(int x, int y) const
{
	if (x < 0 || y < 0 || x >= m_levelData.width || y >= m_levelData.height)
		return false;
	return m_levelData.laddersLayer[x + y * m_levelData.width];
}

bool jet::Level::isInBounds(int x, int y) const
{
	return !((x < 0 || x >= m_levelData.width) || (y < 0 || y >= m_levelData.height));
}

void jet::Level::add(jet::Entity* entity)
{
	if(entity != nullptr)
	{
		entity->setLevel(this);
		entity->init();
		m_entities[entity->getEntityType()].push_back(entity);
		insertEntity(entity->getX() / 32.0f, entity->getY() / 32.0f, entity);
	}
}

void jet::Level::remove(jet::Entity* entity)
{
	if (entity == nullptr)
		return;
	std::vector<jet::Entity*>* entitiesOfType = &m_entities[entity->getEntityType()];

	const auto toRemove = std::find(entitiesOfType->begin(), entitiesOfType->end(), entity);
	if(toRemove != entitiesOfType->end())
	{
		entitiesOfType->erase(toRemove);
	}
	const int tx = static_cast<int>(entity->getX() / 32.0f);
	const int ty = static_cast<int>(entity->getY() / 32.0f);
	removeEntity(tx, ty, entity);
	delete entity;
}

void jet::Level::insertEntity(int tx, int ty, jet::Entity* entity)
{
	if (!isInBounds(tx, ty))
		return;
	m_entitiesInTile[tx + ty * m_levelData.width].push_back(entity);
}

void jet::Level::removeEntity(int tx, int ty, jet::Entity* entity)
{
	if (!isInBounds(tx, ty))
		return;
	auto&& currentTilePos = m_entitiesInTile[tx + ty * m_levelData.width];
	const auto toRemove = std::find(currentTilePos.begin(), currentTilePos.end(), entity);
	if (toRemove != currentTilePos.end())
	{
		currentTilePos.erase(toRemove);
	}
}

void jet::Level::addDamage(jet::Entity* source, float x, float y, float width, float height, int time, int damage)
{
	auto damageEntity = new DamageEntity(source, width, height, time, damage);
	damageEntity->setPosition(x, y);
	damageEntity->setXR(width/2.0f);
	damageEntity->setYR(height/2.0f);
	add(damageEntity);
}


std::vector<jet::Entity*> jet::Level::getEntitiesInTile(int tx, int ty)
{
	return m_entitiesInTile[tx + ty * m_levelData.width];
}

std::vector<jet::Entity*>& jet::Level::getEntitiesOfType(jet::EntityType et)
{
	return m_entities[et];
}

std::vector<jet::Entity*> jet::Level::getEntitiesInRange(int tx, int ty, int rx, int ry)
{
	std::vector<jet::Entity*> entities;
	for (int x = tx - rx; x <= tx + rx; ++x)
	{
		if (x < 0 || x >= m_levelData.width)
			continue;
		for (int y = ty - ry; y <= ty + ry; ++y)
		{
			if (y < 0 || y >= m_levelData.height)
				continue;
			auto&& entitiesInTile = m_entitiesInTile[x + y * m_levelData.width];
			entities.insert(std::end(entities), 
				std::begin(entitiesInTile), std::end(entitiesInTile));
			//entities.push_back(m_entitiesInTile[x + y * m_levelData]);
		}
	}
	return entities;
}

//Considering center as origin of the body
sf::Vector2f jet::Level::handleCollision(const sf::Vector2f& bodyPosition, float sz_x, float sz_y, float v_x, float v_y, jet::ECollisionFlags* collisionFlags)
{
	sf::Vector2f newPosition = bodyPosition;

	static const float offset = 0.0f;

	const float centerX = newPosition.x;
	const float centerY = newPosition.y;
	const int tileX = static_cast<int>(floorf(centerX / 32.0f));
	const int tileY = static_cast<int>(floorf(centerY / 32.0f));

	if(isCollidableAt(tileX, tileY))
	{
		//Check if the entity is inside a block. if true, push him out or allow moving inside it
		if (centerX >= tileX * 32 && centerX <= tileX * 32.0f + 32.0f)
		{
			if (centerY >= tileY * 32 && centerY <= tileY * 32.0f + 32.0f)
			{
				return newPosition;
			}
		}
	}

	//printf("%d\n", tileDown);
	//https://bitbucket.org/AParfen/supermariohd/src/2770a0095a2d8515c09564375d5a2c0e6318c76b/Mario/Mario/Blocks.cpp?at=master&fileviewer=file-view-default
	
	const int tileLeft = static_cast<int>(floorf((newPosition.x - sz_x + 1.5f) / 32.0f));
	const int tileRight = static_cast<int>(floorf((newPosition.x + sz_x - 1.5f) / 32.0f));
	const int tileTop = static_cast<int>(floorf((newPosition.y - sz_y - offset) / 32.0f));
	const int tileDown = static_cast<int>(floorf((newPosition.y + sz_y + offset + 1.0f) / 32.0f));

	if (isCollidableAt(tileLeft, tileDown) || isCollidableAt(tileRight, tileDown))
	{
		if (v_y >= 0)
		{
			newPosition.y = tileDown * 32.0f - sz_y;
			(*collisionFlags) |= jet::ECollisionFlags::Floor;
		}
	}
	if (isCollidableAt(tileLeft, tileTop) || isCollidableAt(tileRight, tileTop))
	{
		if (v_y < 0)
		{
			newPosition.y = tileTop * 32.0f + 32.0f + sz_y;
			(*collisionFlags) |= jet::ECollisionFlags::Ceil;
		}
	}
	
	const float rndmOffset = 0.1f; //Just to be sure we don't get stuck in the wall. 1.0f was good too.

	const int left = static_cast<int>((newPosition.x - sz_x - rndmOffset) / 32.0f);
	if (isCollidableAt(left, static_cast<int>(newPosition.y / 32.0f)))
	{
		newPosition.x = left * 32.0f + 32.0f + sz_x + rndmOffset;
		(*collisionFlags) |= jet::ECollisionFlags::Left;
	}

	const int right = static_cast<int>((newPosition.x + sz_x + rndmOffset) / 32.0f);
	if (isCollidableAt(right, static_cast<int>(newPosition.y / 32.0f)))
	{
		newPosition.x = right * 32.0f - sz_x - rndmOffset;
		(*collisionFlags) |= jet::ECollisionFlags::Right;
	}

	/*for (int tx = tileLeft; tx < tileRight; ++tx)
	{
		for (int ty = tileTop; ty < tileDown; ++ty)
		{
			if (isCollidableAt(tx, ty))
			{
				if (v_x > 0)
				{
					newPosition.x = tx * 32.0f - sz_x - 1.5f;
					(*collisionFlags) |= jet::ECollisionFlags::Right;
				}
				else if (v_x < 0)
				{
					newPosition.x = tx * 32.0f + 32.0f + sz_x + 1.5f;
					(*collisionFlags) |= jet::ECollisionFlags::Left;
				}
			}
		}
	}

	newPosition.x -= v_x;

	tileLeft = ((newPosition.x - sz_x - offset) / 32.0f);
	tileRight = ((newPosition.x + sz_x + offset) / 32.0f);
	tileTop = ((newPosition.y - sz_y - offset) / 32.0f);
	tileDown = ((newPosition.y + sz_y + offset) / 32.0f);

	for (int tx = tileLeft; tx < tileRight; ++tx)
	{
		for (int ty = tileTop; ty <= tileDown; ++ty)
		{
			if (isCollidableAt(tx, ty))
			{
				//Down-wards?
				if (v_y >= 0)
				{
					newPosition.y = ty * 32.0f - sz_y ;
					(*collisionFlags) |= jet::ECollisionFlags::Floor;
				}
				else if (v_y < 0)
				{
					newPosition.y = ty * 32.0f + 32.0f + sz_y;
					(*collisionFlags) |= jet::ECollisionFlags::Ceil;
				}
			}
		}
	}*/
	
	//newPosition.x += v_x;

	return newPosition;
}

std::vector<jet::EntityCollisionData> jet::Level::handleEntityCollision(jet::Entity* entity, sf::Vector2f& processed)
{
	std::vector<jet::EntityCollisionData> entityCollisionData;
	const float entity_x = entity->getX();
	const float entity_y = entity->getY();
	const float entity_sz_x = entity->getXR();
	const float entity_sz_y = entity->getYR();
	const int tx = static_cast<int>(entity_x / 32.0f);
	const int ty = static_cast<int>(entity_y / 32.0f);
	std::vector<jet::Entity*> probablyCollidableEntities = getEntitiesInRange(tx, ty, 1, 1);
	for (auto&& o : probablyCollidableEntities)
	{
		if (o->getEntityType() == _Damage)
		{
			std::cout << "Damage someting" << std::endl;
		}
		if (entity == o)
			continue;
		if(entity->contains(o->getX(), o->getY(), o->getXR(), o->getYR()))
		{
			jet::ECollisionFlags e1Flags = jet::ECollisionFlags::None;
			jet::ECollisionFlags e2Flags = jet::ECollisionFlags::None;
			if (entity_y > o->getY())
			{
				e1Flags |= jet::ECollisionFlags::Top;
				e2Flags |= jet::ECollisionFlags::Bot;
				//processed.y = o->getY() + o->getYR() + entity->getYR();
			}
			if (entity_y < o->getY())
			{
				e1Flags |= jet::ECollisionFlags::Bot;
				e2Flags |= jet::ECollisionFlags::Top;
				//processed.y = o->getY() - o->getYR() - entity->getYR();
			}
			if (entity_x > o->getX())
			{
				e1Flags |= jet::ECollisionFlags::Left;
				e2Flags |= jet::ECollisionFlags::Right;
				//processed.x = o->getX() + o->getXR() + entity->getXR();
			}
			if (entity_x < o->getX())
			{
				e1Flags |= jet::ECollisionFlags::Right;
				e2Flags |= jet::ECollisionFlags::Left;
				//processed.x = o->getX() - o->getXR() - entity->getXR();
			}
			//(*entity->getCollisionFlags()) |= flags;

			entityCollisionData.push_back(jet::EntityCollisionData{ o, e1Flags });
			entity->onCollision(o, e1Flags);
			o->onCollision(entity, e2Flags);
		}
	}
	return entityCollisionData;
}

sf::Vector2f jet::Level::handleMobCollisions(jet::Mob& toProcess)
{
	const sf::Vector2f eVelocity = toProcess.getVelocity();
	sf::Vector2f newPosition =
		handleCollision({ toProcess.getX(), toProcess.getY() },
			toProcess.getXR(), toProcess.getYR(), 
			eVelocity.x, eVelocity.y, 
			toProcess.getCollisionFlags());
	auto collidedEntities = handleEntityCollision(&toProcess, newPosition);
	return newPosition;
}
