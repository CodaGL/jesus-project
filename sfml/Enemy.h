#pragma once
#include "Mob.h"
#include "Player.h"
#include "EntityType.h"

namespace jet
{
	class Enemy : public Mob
	{
	protected:
		jet::Player* m_player;

	public:
		Enemy() = default;

		void init() override;

		void setPlayer(jet::Player* player);

		jet::Player* getPlayer() const;

		jet::EntityType getEntityType() const override;
	};
}
