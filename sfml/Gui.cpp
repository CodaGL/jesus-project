#include "Gui.h"

#include <SFML/Graphics/RenderTarget.hpp>

jet::Gui::~Gui()
{
	for(auto&& widget : m_widgets)
	{
		delete widget;
	}
}


void jet::Gui::update(const sf::Time& time)
{
	for (auto&& widget : m_widgets)
	{
		widget->update(time);
	}
}

void jet::Gui::add(Widget* widget)
{
	if (widget == nullptr)
		return;
	m_widgets.push_back(widget);
}

void jet::Gui::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	for(auto&& widget : m_widgets)
	{
		target.draw(*widget);
	}
}
