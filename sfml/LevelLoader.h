#pragma once


struct LevelData;
namespace jet
{
	//This class is used after LevelReader work.
	//Reader: reads the tiles, layers, objects, etc and store them in a LevelData
	//Loader: takes the LevelData, process them and create a Level*.
	class Level;

	class LevelLoader
	{
	public:
		void loadLevel(LevelData& levelData, Level& level);

		void loadObjects(LevelData& levelData, Level& level);

	private:
		void loadLevelEnemies(LevelData& levelData, Level& level);
		void loadLights(LevelData& levelData, Level& level);
	};
}
