#pragma once
#include <string>
#include "json.hpp"
#include <iostream>
#include <fstream>
#include "JsonHelper.h"

using json = nlohmann::json;

namespace jet
{
	struct ItemData final
	{
		std::string name;

		//For example: sword is a base name. It will take textures with: left_hand_(textureBaseName), right_hand_(textureBaseName). Example: sword is right_hand_sword
		std::string baseName; 

		//FRAMES ETC
		int idleFrames;
		int walkingFrames = 0;
		int jumpingFrames = 0;
		int inAirFrames = 0;
		int ladderIdleFrames = 0;
		int ladderMovingFrames = 0;
		int attackFrames = 0;

		bool loadFromFile(const std::string& path);
	};

	inline bool ItemData::loadFromFile(const std::string& file)
	{
		std::ifstream i(file);
		json j;
		i >> j;

		name = getJsonValue<std::string>(j, "name", "Undefined");
		baseName = getJsonValue<std::string>(j, "baseName", "Undefined");
		if(j.find("frames") != j.end())
		{
			auto& framesRef = j;//j["frames"];
			idleFrames = getJsonValue<int>(framesRef, "frames/idle", 0);
			walkingFrames = getJsonValue<int>(framesRef, "frames/walking", 0);
			jumpingFrames = getJsonValue<int>(framesRef, "frames/jumping", 0);
			inAirFrames = getJsonValue<int>(framesRef, "frames/inAir", 0);
			ladderIdleFrames = getJsonValue<int>(framesRef, "frames/ladderIdle", 0);
			ladderMovingFrames = getJsonValue<int>(framesRef, "frames/ladderMoving", 0);
			attackFrames = getJsonValue<int>(framesRef, "frames/attack", 0);
		}
		return true;
	}

}
