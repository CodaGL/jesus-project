#include "AnimatedEntity.h"
#include "globals.h"

#include <cassert>
#include <iostream>


jet::AnimatedEntity::AnimatedEntity()
{
	m_animatedSprite.setOrigin(48.0f/2.0f, (48.0f+16)/2.0f);
}


jet::AnimatedEntity::~AnimatedEntity()
{
	for(auto&& mK : m_animations)
	{
		delete mK.second;
	}
}

void jet::AnimatedEntity::update(const sf::Time& time)
{
	m_animatedSprite.update(time);
	if(m_coloredTime > sf::Time::Zero)
	{
		m_coloredTime -= time;
		if(m_coloredTime <= sf::Time::Zero)
		{
			m_animatedSprite.setColor(sf::Color::White);
			m_currentSpriteColor = sf::Color::White;
		}
	}
	jet::Entity::update(time);
}


void jet::AnimatedEntity::render(sf::RenderTarget& target) const
{
	target.draw(m_animatedSprite);
	jet::Entity::render(target);
}


void jet::AnimatedEntity::setAnimation(const Animation* animation)
{
	if (animation == nullptr)
		return;
	m_animatedSprite.setAnimation(animation);
}

void jet::AnimatedEntity::addAnimation(EntityState entityState, jet::Animation* anim)
{
	if (anim == nullptr)
		return;
	if(anim->getAnimationTime() == sf::Time::Zero)
	{
		delete anim;
		return;
	}
	if(m_animations.find(entityState) != m_animations.end())
	{
		delete m_animations[entityState];
	}
	m_animations[entityState] = anim;
}

const jet::Animation* jet::AnimatedEntity::getAnimation(EntityState entityState) const
{
	if(m_animations.find(entityState) == m_animations.end())
	{
		return getAnimation(EntityState::Idle); //nullptr?
	}
	return m_animations.at(entityState);
}


void jet::AnimatedEntity::setEntityState(EntityState e)
{
	setEntityState(e, false); 
}

void jet::AnimatedEntity::setEntityState(EntityState e, bool updateAnimation)
{
	if (m_entityState == e)
		return;
	Entity::setEntityState(e);
	if(updateAnimation)
	{
		setAnimation(getAnimation(e));
		m_currentAnimationEntityState = e;
	}
}

void jet::AnimatedEntity::setPosition(float x, float y)
{
	jet::Entity::setPosition(x, y);
	m_animatedSprite.setPosition(x, y);
}

void jet::AnimatedEntity::setSpriteColor(const sf::Color& color, const sf::Time& time)
{
	if (time <= sf::Time::Zero)
		return;
	m_animatedSprite.setColor(color);
	m_coloredTime = time;
	m_currentSpriteColor = color;
}

bool jet::AnimatedEntity::isPlaying(EntityState state) const
{
	return m_currentAnimationEntityState == state && m_animatedSprite.isPlaying();
}


const sf::Color& jet::AnimatedEntity::getSpriteColor() const
{
	return m_currentSpriteColor;
}

size_t jet::AnimatedEntity::getAnimationFrame() const
{
	return m_animatedSprite.getCurrentFrame();
}
