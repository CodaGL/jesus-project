#include "BitmapFont.h"

bool jet::BitmapFont::loadFromFile(const std::string& path)
{
	if (path.empty())
		return false;
	if(!m_texture.loadFromFile(path))
	{
		return false;
	}
	return true;
}


sf::Texture* jet::BitmapFont::getTexture()
{
	return &m_texture;
}
