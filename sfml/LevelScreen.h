#pragma once
#include "Screen.h"
#include "Player.h"
#include "LevelData.h"
#include "Level.h"
#include "BitmapText.h"
#include "DebugRenderer.h"
#include "Gui.h"
#include "NinePatch.h"

namespace jet
{
	class TileMap;
	class LevelRenderer;
	
	class LevelScreen : public Screen
	{
	public:
		LevelScreen();
		~LevelScreen();

		void onEnter() override;
		virtual void onExit() override;

		void tick() override;
		void update(const sf::Time& time) override;

		void render(sf::RenderTarget& target) override;
		void renderUI(sf::RenderTarget& target) override;


		void resize(int width, int height) override;

		bool switchLevel(const std::string& name);

		LevelData& getLevelData();

	private:
		sf::View m_view;
		sf::View m_guiView;

		Gui m_gui;

		Widget* m_mouseFollower;
		NinePatch* m_ninePatch;

		Level* m_level;
		LevelData m_levelData;

		Player* m_player;

		LevelRenderer* m_levelRenderer;
		DebugRenderer* m_debugRenderer;

		BitmapText m_bitmapText;
	};
}
