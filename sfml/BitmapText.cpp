#include "BitmapText.h"
#include <SFML/Graphics/RenderTarget.hpp>
#include <iostream>
#include "Utility.h"
#include "globals.h"

const int TAB_SIZE = 4.0f; //\t

jet::BitmapText::BitmapText() :
	m_vertices(sf::PrimitiveType::Quads),
	m_color(sf::Color::Red)
{
	m_font = g_resourceManager->getBitmapFont("default_12");
}

void jet::BitmapText::rebuild()
{
	if(m_string.empty())
	{
		m_vertices.clear();
		return;
	}
	m_vertices.clear();

	toUpperCase(m_string);

	float _x = 0.0f;
	float _y = 0.0f;
	float characterSize = 12.0f;
	float padding = 1.0f;

	sf::Color currentColor = m_color;

	for(size_t i = 0; i < m_string.length(); ++i)
	{
		unsigned char ch = m_string.at(i);
		if(ch == '[') //Possible color markup
		{
			int toSkip = parseMarkupLanguage(m_string, currentColor, i + 1, m_string.length());
			if(toSkip >= 0)
			{
				i += toSkip;
				continue;
			}
			else if(toSkip == -2) //Escaping [. Don't make anything.
			{
				std::cout << "Escape" << std::endl;
			}
		}
		else if(ch == '\n')
		{
			_x = 0.0f;
			_y += characterSize + padding;
			continue;
		}
		else if(ch == '\t')
		{
			_x += characterSize * TAB_SIZE;
			continue;
		}
		ch -= ' '; //
		const sf::Vector2f uv((ch % 16)*characterSize, (ch / 16)*characterSize);
		m_vertices.append(sf::Vertex{ { _x, _y }, currentColor,{ uv.x, uv.y } });
		m_vertices.append(sf::Vertex{ { _x + characterSize, _y }, currentColor,{ uv.x + characterSize, uv.y } });
		m_vertices.append(sf::Vertex{ { _x + characterSize, _y + characterSize }, currentColor,{ uv.x + characterSize, uv.y + characterSize } });
		m_vertices.append(sf::Vertex{ { _x, _y + characterSize }, currentColor,{ uv.x, uv.y + characterSize } });
		_x += (characterSize + padding);
	}
	//setColor(m_color);
}


void jet::BitmapText::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	states.texture = m_font->getTexture();
	target.draw(m_vertices, states);
}

int jet::BitmapText::parseMarkupLanguage(const std::string& str, sf::Color& currentColor, int start, int end)
{
	if (start == end)
		return -1;
	unsigned char nextCh = str.at(start);
	int colorInt = 0;

	switch (nextCh)
	{
	case '[': //Escaping [ in string.
		return -2;
	case '#':
		for (int ci = start+1; ci < end; ++ci)
		{
			unsigned char ch = str.at(ci);
			if(ch == ']')
			{
				if (ci == start + 6) //Must add Alpha.
				{
					colorInt = colorInt * 16 + 15;
					colorInt = colorInt * 16 + 15;
				}
				else if(ci < start + 8 || ci > start + 9)
				{
					break;
				}
				currentColor.r = (colorInt & 0xFF000000) >> 24;
				currentColor.g = (colorInt & 0x00FF0000) >> 16;
				currentColor.b = (colorInt & 0x0000FF00) >> 8;
				currentColor.a = (colorInt & 0x000000FF);
				return (ci - start) + 1;
			}

			if(ch >= '0' && ch <= '9')
			{
				colorInt = (colorInt << 4) + (ch - '0');
			}
			else if (ch >= 'a' && ch <= 'f')
			{
				colorInt = (colorInt << 4) + (ch - ('a' - 10));
			}
			else if (ch >= 'A' && ch <= 'F')
			{
				colorInt = (colorInt << 4) + (ch - ('A' - 10));
			}
		}
		return -1;
		break;
	}
	
	//Try to check if it's a named color.
	/*size_t pos = str.find(']');
	if(pos != std::string::npos)
	{

		std::string name = str.substr(start, pos-start);
		sf::Color color = getColorFromName(name);
		currentColor.r = color.r;
		currentColor.g = color.g;
		currentColor.b = color.b;
		currentColor.a = color.a;
		return (pos - start) + 1;
	}*/
	for(int i = start; i < end; ++i)
	{
		unsigned char ch = str.at(i);
		
		if (ch == '[') //Special Case. Could happen when open a tag and not closed.
			break;
		
		if (ch != ']')
			continue;
		std::string name = str;
		name = name.substr(start, i - start);
		sf::Color color = getColorFromName(name);
		currentColor.r = color.r;
		currentColor.g = color.g;
		currentColor.b = color.b;
		currentColor.a = color.a;
		return (i - start)+1;
	}
	return -1;
}

sf::Color jet::BitmapText::getColorFromName(const std::string& name)
{
	std::string lower = name;
	toLowerCase(lower);
	

	if (lower == "green")
	{
		return sf::Color::Green;
	}
	else if (lower == "blue")
	{
		return sf::Color::Blue;
	}
	else if (lower == "red")
	{
		return sf::Color::Red;
	}
	else if (lower == "yellow")
	{
		return sf::Color::Yellow;
	}
	else if(lower == "white")
	{
		return sf::Color::White;
	}
	return sf::Color::White;
}


/*
void jet::BitmapText::addGlyph(float x, float y, char character, float characterSize)
{
	
}
*/

void jet::BitmapText::setColor(const sf::Color& color)
{
	m_color = color;
	for(size_t i = 0; i < m_vertices.getVertexCount(); ++i)
	{
		m_vertices[i].color = color;
	}
}

const sf::Color& jet::BitmapText::getColor() const
{
	return m_color;
}

void jet::BitmapText::setString(const std::string& string)
{
	m_string = string;
	rebuild();
}

const std::string& jet::BitmapText::getString() const
{
	return m_string;
}