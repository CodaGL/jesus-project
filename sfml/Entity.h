#pragma once
#include <map>
#include <typeindex>
#include <cstdint>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include "LineRectangle.h"
#include <SFML/System/Time.hpp>
#include "Screen.h"
#include "EntityType.h"


namespace jet 
{
	class World;
	class Level;
	class EntityEquipment;

	typedef uint16_t EntityId;
	
	enum EntityState
	{
		VOID = 0,
		Idle,
		Moving,
		Jumping, //3
		InAir,
		OnLadder_Idle,
		OnLadder_Moving,
		Attacking
	};

	enum class ECollisionFlags : uint32_t
	{
		None = 0,

		Floor = 1 << 0,
		Ceil = 1 << 1,
		Left = 1 << 2,
		Right = 1 << 3,

		Bot = Floor,
		Top = Ceil,

	};

	class Entity
	{
	private:

	protected:
		Level* m_level;

		bool m_isAlive;
		
		float m_x, m_y;
		float m_xr, m_yr; //Entity size
		
		bool m_onGround;
		bool m_wasOnGround;

		bool m_isCollidableX = true;
		bool m_isCollidableY = true;

		ECollisionFlags m_collisionFlags;

		std::vector<EntityEquipment*> m_equipment;

	public:

		Entity() : m_isAlive(true) {};
		virtual ~Entity() = default;

		virtual void init() {};

		virtual void processCollisions(){}

		virtual void update(const sf::Time& time) {}

		virtual void tick(){}
		virtual void postTick() {}

		virtual void render(sf::RenderTarget& target) const;

		virtual void renderDebug(sf::RenderTarget& target) const;

		EntityState getEntityState() const;
		virtual void setEntityState(EntityState es);

		virtual void setPosition(float x, float y)
		{
			m_x = x;
			m_y = y;
		}

		const float getX() const;
		void setX(float _x);

		const float getY() const;
		void setY(float _y);

		const float getLeft() const;
		const float getRight() const;
		
		const float getCenterX() const;
		const float getCenterY() const;
		const float getBottom() const;
		const float getTop() const;
		
		const float getXR() const;
		void setXR(float _xr);

		const float getYR() const;
		void setYR(float _yr);

		void setLevel(Level* level);
		Level* getLevel();

		bool contains(float x, float y, float sz_x, float sz_y) const;

		virtual void onCollision(Entity* entity, ECollisionFlags collisionFlag) {};

		jet::ECollisionFlags* getCollisionFlags()
		{
			return &m_collisionFlags;
		}

		virtual jet::EntityType getEntityType() const = 0;
	
		bool isAlive() const
		{
			return m_isAlive;
		}
	protected:
		EntityState m_entityState;

	};

	/*template<typename C, typename V, typename... C2>
	inline bool Entity::hasComponent()
	{
		return hasComponent<C>() && hasComponent<V, C2...>();
	}*/

	inline constexpr enum jet::ECollisionFlags operator|(const enum jet::ECollisionFlags a, const enum jet::ECollisionFlags b)
	{
		return ECollisionFlags(static_cast<uint32_t>(a) | static_cast<uint32_t>(b));
	}

	inline constexpr enum jet::ECollisionFlags operator|=(jet::ECollisionFlags& a, const jet::ECollisionFlags& b)
	{
		a = ECollisionFlags(static_cast<uint32_t>(a) | static_cast<uint32_t>(b));
		return a;//static_cast<jet::ECollisionFlags>(static_cast<int>(a) | static_cast<int>(b));
	}

	inline constexpr bool operator&(const enum ECollisionFlags a, const enum ECollisionFlags b)
	{
		return static_cast<uint32_t>(a) & static_cast<uint32_t>(b);
	}
}
