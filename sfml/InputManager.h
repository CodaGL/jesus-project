#pragma once
#include "Singleton.h"
#include <SFML/Graphics/RenderWindow.hpp>
#include <map>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Graphics/RenderTexture.hpp>
#include <SFML/Window/Mouse.hpp>

class InputManager
{
public:
	enum Key
	{
		START = 0,

		WalkLeft,
		WalkRight,
		Jump,
		LadderUp,
		LadderDown,
		//Used by Level Editor Screen
		MoveUp,
		MoveDown,
		MoveLeft,
		MoveRight,
		//
		Attack,
		LAST
	};

public:
	InputManager();
	~InputManager();


	void setWindow(sf::RenderWindow* window, sf::RenderTexture* renderTexture);

	void update();

	bool isKeyJustPressed(Key k);
	bool isKeyPressed(Key k);

	const sf::Vector2f getGameMousePosition() const;
	const sf::Vector2f getGuiMousePosition() const;

	const sf::Vector2f getMousePosition(const sf::View& view) const;
	const sf::Vector2f getMousePosition() const;

	bool isMouseButtonPressed(sf::Mouse::Button b) const;
	bool isLeftMouseButtonJustPressed() const;

	void setGameView(sf::View& gameView);
	void setGuiView(sf::View& guiView);

private:
	sf::View* m_gameView; //REF
	sf::View* m_guiView; //REF

	sf::RenderWindow* m_window;
	sf::RenderTexture* m_renderTexture;
	sf::Vector2f m_windowScale;

	std::map<Key, bool> m_keyActiveMap;
	std::map<Key, bool> m_justPressedKeyMap;

	std::map<Key, sf::Keyboard::Key> m_keysMap;

	bool m_isWindowFocused;

	bool m_leftMousePressed;
	bool m_leftMouseJustPressed;
private:
	void initialize();

	bool isKeyPressed(sf::Keyboard::Key k);
};
