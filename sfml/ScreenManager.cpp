#include "ScreenManager.h"
#include "Screen.h"
#include <SFML/System/Time.hpp>

namespace jet
{
	ScreenManager::ScreenManager(Screen* initialScreen)
		: m_nextScreen(nullptr)
	{
		m_currentScreen = initialScreen;
		m_currentScreen->setScreenManager(this);
		m_currentScreen->onEnter();
	}

	ScreenManager::~ScreenManager()
	{
		delete m_currentScreen;
	}

	void ScreenManager::tick()
	{
		m_currentScreen->tick();
		if (m_nextScreen != nullptr)
		{
			m_currentScreen->onExit();
			delete m_currentScreen;

			m_currentScreen = m_nextScreen;
			m_currentScreen->onEnter();

			m_nextScreen = nullptr;
		}
	}

	void ScreenManager::update(const sf::Time& time)
	{
		m_currentScreen->update(time);
	}


	void ScreenManager::render(sf::RenderTarget& target)
	{
		m_currentScreen->render(target);
		m_currentScreen->renderUI(target);
	}

	void ScreenManager::setNextScreen(Screen* screen)
	{
		delete m_nextScreen;

		m_nextScreen = screen;
		m_nextScreen->setScreenManager(this);
	}

	void ScreenManager::resize(int width, int height)
	{
		m_currentScreen->resize(width, height);
	}

}