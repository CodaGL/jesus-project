#pragma once
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/System/Time.hpp>

namespace jet
{
	class Animation;

	class AnimatedSprite : public sf::Drawable, public sf::Transformable
	{
	public:
		
		AnimatedSprite();

		void play();
		void play(Animation* animation);
		void pause();
		void stop();

		void setAnimation(const Animation* animation);

		void setFrame(size_t id);
		void setColor(const sf::Color& color);

		void update(const sf::Time& time);

		void setFlippedX(bool flip);
		bool isFlippedX() const;

		void setFlippedY(bool flip);
		bool isFlippedY() const;

		bool isLooped() const;
		bool isPlaying() const;

		size_t getCurrentFrame() const
		{
			return m_currentFrame;
		}

	private:
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	private:

		//Current Animation Info
		const Animation* m_currentAnimation;
		sf::Texture* m_texture;
		bool m_isLoop;
		sf::Time m_frameTime;

		//! Ot
		sf::Time m_currentTime;
		size_t m_currentFrame;

		bool m_isPaused;
		
		bool m_isFlippedX;
		bool m_isFlippedY;

		sf::VertexArray m_vertices;
	};
}
