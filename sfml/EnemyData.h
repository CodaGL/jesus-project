#pragma once
#include "EnemyID.h"
#include <SFML/System/Vector2.hpp>

namespace jet
{
	struct EnemyData final
	{
		EnemyID id;
		
		int objectId;

		sf::Vector2f spawnPosition;
	};
}
