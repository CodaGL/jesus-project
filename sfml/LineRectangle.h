#pragma once
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/VertexArray.hpp>

namespace Utility
{
	class LineRectangle : public sf::Drawable, public sf::Transformable
	{
	public:

		enum class RectangleSide : uint8_t
		{
			Top = 0, Right = 1, Bot = 2, Left = 3, Up = Top, Down = Bot
		};

		LineRectangle(const sf::Vector2f& position, const sf::Vector2f& size, const std::vector<sf::Color>& colors = { sf::Color::White, sf::Color::White, sf::Color::White, sf::Color::White });

		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

		void setTickness(const sf::Vector2f& tickness);
		const sf::Vector2f& getTickness() const;

		void setSideColor(const RectangleSide& side, const sf::Color& color);
	private:

		sf::VertexArray m_vertices;

		sf::Vector2f m_size;
		sf::Vector2f m_tickness;

		std::vector<sf::Color> m_colors;

		void rebuild();
	};
}