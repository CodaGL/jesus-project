#include "Player.h"
#include <SFML/Window/Keyboard.hpp>
#include "ResourceManager.h"
#include <imgui.h>
#include "globals.h"
#include "InputManager.h"
#include "Level.h"
#include "Math.h"
#include "Enemy.h"
#include "DamageEntity.h"

jet::Player::Player()
	:
	m_jumpTicks(0),
	m_isOnLadder(false)
{
	m_x = 64;
	m_y = 32*32;
	m_xr = 6.0f;
	m_yr = 16.0f;
	m_direction = RIGHT;

	loadAnimations("jesus_spritesheet", 48, 48);
	m_animatedSprite.setOrigin(48.0f / 2.0f, (48.0f + 16.0f) / 2.0f);
	m_maxHealth = 10;
	m_health = m_maxHealth;

	setEntityState(EntityState::Idle);
}

jet::Player::~Player()
{
	
}

void jet::Player::handleInput()
{
	/*		LADDERS		*/		
	if (g_inputManager->isKeyPressed(InputManager::Key::LadderUp))
	{
		if(m_isOnLadder)
		{
			m_velocity.y = -1.0f;
		}
		else
		{
			if(isOnLadder(false))
			{
				enterLadder();
				m_y -= 1.6f;
			}
		}
	}
	else if (g_inputManager->isKeyPressed(InputManager::Key::LadderDown))
	{
		if(m_isOnLadder)
		{
			m_velocity.y = 1.0f;
		}
		else
		{
			if(isOnLadder(true))
			{
				m_y += 1.6f;
				enterLadder();
			}
		}
	}
	else if(m_isOnLadder)
	{
		m_velocity.y = 0.0f;
	}
	/////////////////
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::R))
	{
		damage(1, nullptr);
	}
	if (g_inputManager->isKeyPressed(InputManager::Key::WalkLeft))
	{
		m_acceleration.x = -kAccelerationX * 
			((m_isOnLadder) ? (kAccelerationXOnLadderSlowdown) : (1.0f));

		if(m_velocity.x > 0)
			m_velocity.x = m_acceleration.x;
		m_direction = Direction::LEFT;
	}
	else if (g_inputManager->isKeyPressed(InputManager::Key::WalkRight))
	{
		m_acceleration.x = kAccelerationX * 
			((m_isOnLadder) ? (kAccelerationXOnLadderSlowdown) : (1.0f));

		if (m_velocity.x < 0)
			m_velocity.x = m_acceleration.x;
		m_direction = Direction::RIGHT;
	}
	else
	{
		m_acceleration.x = 0.0f;
		m_velocity.x = 0.0f;
	}

	
	if (g_inputManager->isKeyPressed(InputManager::Key::Jump))
	{
		jump();
	}

	if(g_inputManager->isKeyPressed(InputManager::Key::Attack))
	{
		attack();
	}
}


/**/
//https://www.gamedev.net/forums/topic/509143-how-to-go-about-platformer-physics/
//http://www.lua.org/pil/12.html
//https://forums.tigsource.com/index.php?topic=21997.20
//https://www.gamedev.net/forums/topic/691218-c-and-sdl2-tile-collision/
//https://gamedev.stackexchange.com/questions/22327/how-can-i-define-a-complex-collision-area-on-top-of-a-tile-map
//http://info.sonicretro.org/SPG:Solid_Tiles
//Others: https://github.com/librg/librg

/**/
void jet::Player::processCollisions()
{
	Mob::processCollisions();

	if (m_collisionFlags & ECollisionFlags::Ceil)
	{
		if (m_isJumping || m_jumpTicks > 0)
		{
			stopJump();
		}
	}

	if(m_isOnLadder)
	{
		if(m_onGround && !isOnLadder(true))
		{
			exitLadder();
		}
		else if(!isOnLadder() && !isOnLadder(true))
		{
			exitLadder();
		}
	}
}

void jet::Player::onCollision(Entity* entity, ECollisionFlags collisionFlag)
{
}


void jet::Player::postTick()
{
	jet::Mob::postTick();
}

void jet::Player::tick()
{
	const EntityState _prevState = getEntityState();
	EntityState _newState = _prevState;

	m_wasOnGround = m_onGround;
	

	jet::Mob::tick();

	m_velocity.x += m_acceleration.x;
	
	if(m_lastHitTicks > 0)
	{
		m_lastHitTicks--;
	}

	if(m_isOnLadder)
	{
	}
	else if (m_isJumping && m_jumpTicks > 0)
	{
		m_jumpTicks--;
		m_velocity.y = -kJumpAcceleration;
		m_velocity.y = Math::clamp(m_velocity.y, -kMaxAccelerationY, 0.0f);
		//m_y += m_velocity.y;
		m_applyGravity = false;
	}
	else if (m_onGround)
	{
		m_isJumping = false;
		m_applyGravity = false;
	}
	else
	{
		m_applyGravity = true;
	}

	if(m_attackTime > 0)
	{
		m_attackTime--;
	}
	//===========================================================================
	//State update
	if(m_isOnLadder)
	{
		_newState = EntityState::OnLadder_Moving;
		if (abs(m_velocity.x) > 0.0f || abs(m_velocity.y) > 0.0f)
		{
			m_animatedSprite.play();
			//_newState = EntityState::OnLadder_Moving;
		}
		else
		{
			m_animatedSprite.pause();
		}
	}
	else if(m_onGround)
	{
		_newState = abs(m_velocity.x) > 0.0f ? EntityState::Moving : EntityState::Idle;
	}
	else //Jumping/InAir
	{
		_newState = EntityState::InAir;
	}

	if(m_isAttacking)
	{
		_newState = EntityState::Attacking;
		if (!isPlaying(EntityState::Attacking))
		{
			m_isAttacking = false;
		}
	}

	if (_newState != _prevState)
	{
		setEntityState(_newState, true);
	}
}

void jet::Player::update(const sf::Time& time)
{
	jet::AnimatedEntity::update(time);
}


void jet::Player::render(sf::RenderTarget& target) const
{
	jet::AnimatedEntity::render(target);
}

void jet::Player::renderGUI() const 
{
	if(ImGui::Begin("Player Info"))
	{
		float t = m_x / 32.0f;
		float t2 = m_y / 32.0f;
		char info[50];
		//sprintf_s(info, "Position(X, Y): %f | %f", static_cast<float>(t), static_cast<float>(t2));
		//ImGui::Text(info);

		sprintf_s(info, "Acceleration(X, Y): %f | %f", m_acceleration.x, m_acceleration.y);
		ImGui::Text(info);

		sprintf_s(info, "Velocity(X, Y): %f | %f", m_velocity.x, m_velocity.y);
		ImGui::Text(info);

		sprintf_s(info, "Jumping: %s", (m_isJumping && m_jumpTicks > 0) ? "true" : "false");
		ImGui::Text(info);

		sprintf_s(info, "Grounded: %s", m_onGround ? "true" : "false");
		ImGui::Text(info);
		
		sprintf_s(info, "Ceiled: %s", m_collisionFlags & ECollisionFlags::Ceil ? "true" : "false");
		ImGui::Text(info);

		sprintf_s(info, "Left: %s", m_collisionFlags & ECollisionFlags::Left ? "true" : "false");
		ImGui::Text(info);

		sprintf_s(info, "Right: %s", m_collisionFlags & ECollisionFlags::Right ? "true" : "false");
		ImGui::Text(info);

		sprintf_s(info, "Is On Ladder: %s", m_isOnLadder ? "true" : "false");
		ImGui::Text(info);
	}
	ImGui::End();
}

bool jet::Player::jump()
{
	if( (m_onGround/* || m_isOnLadder*/) && !m_isJumping)
	{
		/*if(m_isOnLadder)
		{
			exitLadder();
		}*/
		m_jumpTicks = kJumpTicks;
		m_acceleration.y = -kJumpAcceleration;
		m_isJumping = true;
		m_onGround = false;
		return true;
	}
	return false;
}

void jet::Player::stopJump()
{
	m_jumpTicks = 0;
	m_acceleration.y = 0.0f;
	m_velocity.y = 0.0f;
	m_isJumping = false;
}

bool jet::Player::attack()
{
	if (m_isOnLadder || m_attackTime > 0 || m_isAttacking)
		return false;


	sf::FloatRect weaponCollisionRect;

	if (m_direction == LEFT)
	{
		weaponCollisionRect.width = 16.0f;//RANGE?
		weaponCollisionRect.height = 8.0f;//RANGE?
		weaponCollisionRect.left = m_x - m_xr - weaponCollisionRect.width/2.0f;
		weaponCollisionRect.top = m_y;
	}
	else if (m_direction == RIGHT)
	{
		weaponCollisionRect.width = 16.0f;//RANGE?
		weaponCollisionRect.height = 8.0f;//RANGE?
		weaponCollisionRect.left = m_x + m_xr + weaponCollisionRect.width / 2.0f;
		weaponCollisionRect.top = m_y;
	}
	
	m_level->addDamage(this, weaponCollisionRect.left, weaponCollisionRect.top, weaponCollisionRect.width, weaponCollisionRect.height);

	m_isAttacking = true;
	m_attackTime = 60;
	setEntityState(EntityState::Attacking, true);
	return true;
}


bool jet::Player::isOnLadder(bool checkFoot)
{
	bool onLadder = false;
	if(checkFoot)
	{
		onLadder = m_level->isLadder(static_cast<int>(m_x / 32.0f), static_cast<int>((m_y + m_yr) / 32.0f));
	}
	else
	{
		onLadder = m_level->isLadder(static_cast<int>(m_x / 32.0f), static_cast<int>(m_y / 32.0f));
	}
	return onLadder;
}

void jet::Player::enterLadder()
{
	if (m_isOnLadder)
		return;
	m_isOnLadder = true;
	m_isJumping = false;
	m_jumpTicks = 0;
	m_applyGravity = false;
	m_x = (m_x / 32.0f)*32.0f;
	//m_y = m_y - 2.0f;
	m_velocity.y = 0.0f;
	m_acceleration.y = 0.0f;
	//m_isCollidableX = false;
	m_isCollidableY = false;
}

void jet::Player::exitLadder()
{
	if (!m_isOnLadder)
		return;
	m_velocity.y = 0.0f;
	m_isOnLadder = false;
	m_isCollidableX = true;
	m_isCollidableY = true;
	m_applyGravity = true;
}

void jet::Player::damage(int health, Entity * damagedBy)
{
	if (m_lastHitTicks <= 0)
	{
		jet::Mob::damage(health, damagedBy);
		m_lastHitTicks = 2 * 60;
	}
}

jet::EntityType jet::Player::getEntityType() const
{
	return jet::EntityType::_Player;
}
