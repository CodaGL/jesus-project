#pragma once
#include <vector>
#include "TileMap.h"
#include "LightData.h"
#include <unordered_map>
#include <sstream>
#include "EnemyData.h"

template<typename T>
inline T convertTo(const std::string& str)
{
	std::istringstream ss(str);
	T value;
	ss >> value;
	return value;
}

struct LevelData
{
	std::string name;

	int width, height;
	
	float lightLevel;

	std::vector<bool> collidableTiles;
	std::vector<bool> laddersLayer;

	std::vector<std::vector<jet::TileData>> backgroundLayers;
	std::vector<std::vector<jet::TileData>> middlegroundLayers;
	std::vector<std::vector<jet::TileData>> foregroundLayers;

	std::vector<jet::LightData> lights;
	std::vector<jet::EnemyData> enemies;


	bool hasProperty(const std::string& propertyName)
	{
		return !(m_properties.find(propertyName) == m_properties.end());
	}

	template<typename T>
	T getProperty(const std::string& propertyName)
	{
		if(m_properties.find(propertyName) == m_properties.end())
		{
			return T();
		}
		return convertTo<T>(m_properties[propertyName]);
	}

private:
	friend class LevelReader;

	std::unordered_map<std::string, std::string> m_properties;
};
	


/*
template<typename T>
inline void setLevelProperty(LevelData& levelData, std::string propertyName, T value)
{
	if(propertyName.find("light_level") != std::string::npos)
	{
		levelData.lightLevel = value;
	}
}
*/