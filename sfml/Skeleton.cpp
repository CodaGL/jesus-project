#include "Skeleton.h"
#include "globals.h"
#include "ResourceManager.h"
#include "LineRectangle.h"
#include "Player.h"
#include "ObjectFactory.h"
#include "EnemyID.h"

REGISTER_ENEMY(jet::EnemyID::Skeleton_1, jet::Skeleton)

jet::Skeleton::Skeleton()
{
	m_xr = 6.0f;
	m_yr = 16.0f;
	m_health = m_maxHealth = 10;
	m_applyGravity = true;
	m_behaviorTicks = 0;
	m_scriptedBehavior = new ScriptedBehavior("res/scripts/test.nut", this);

	loadAnimations("skeleton_spritesheet", 32.0f, 32.0f);
	m_animatedSprite.setOrigin(32.0f / 2.0f, 32.0f / 2.0f);

	setEntityState(EntityState::Idle);
}

void jet::Skeleton::tick()
{
	const EntityState _prevState = getEntityState();
	EntityState _newState = _prevState;

	if(m_knockBackTicks > 0)
	{
		m_velocity.x = 0;
		m_velocity.y = 0;
	}
	m_velocity.x = 0.0f;
	Mob::tick();

	if(m_onGround)
	{
		if(!m_wasOnGround)
		{
			//m_acceleration.x = -(1.0f / 128.0f);
			m_velocity.x = -kMaxAccelerationX;
		}
		if (m_velocity.x < 0.0f && m_collisionFlags & ECollisionFlags::Left)
		{
			//m_acceleration.x = (1.0f / 128.0f);
			m_velocity.x = kMaxAccelerationX;
		}
		else if (m_velocity.x > 0.0f && m_collisionFlags & ECollisionFlags::Right)
		{
			//m_acceleration.x = -(1.0f / 128.0f);
			m_velocity.x = -kMaxAccelerationX;
		}
		//m_velocity.x += m_acceleration.x;
		m_applyGravity = false;
	}
	else
	{
		m_applyGravity = true;
	}

	if(abs(m_velocity.x) > 0.0f)
	{
		if(m_velocity.x < 0.0f)
		{
			m_direction = LEFT;
		}
		else if(m_velocity.x > 0.0f)
		{
			m_direction = RIGHT;
		}
		_newState = EntityState::Moving;
	}
	else
	{
		_newState = EntityState::Idle;
	}

	if(_newState != _prevState)
	{
		setEntityState(_newState, true);
	}
}

void jet::Skeleton::update(const sf::Time& time)
{
	jet::AnimatedEntity::update(time);
}


void jet::Skeleton::render(sf::RenderTarget& target) const
{
	jet::Mob::render(target);
}

void jet::Skeleton::onCollision(Entity* entity, ECollisionFlags collisionFlag)
{
	//Bad BAD BAD
	auto mob = dynamic_cast<Player*>(entity);
	if(mob != nullptr)
	{
		mob->damage(1, this);
		mob->setKnockback((m_velocity.x > 0.0f ? 1.0f : -1.0f) * 1.5f, -1.5f, 30);
	}
	m_acceleration.x = -m_acceleration.x;
	m_velocity.x = -m_velocity.x;
}
