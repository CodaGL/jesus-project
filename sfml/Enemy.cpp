#include "Enemy.h"
#include "Level.h"

void jet::Enemy::init()
{
	setPlayer(&m_level->getPlayer());
}

void jet::Enemy::setPlayer(jet::Player* player)
{
	m_player = player;
}

jet::Player* jet::Enemy::getPlayer() const
{
	return m_player;
}

jet::EntityType jet::Enemy::getEntityType() const
{
	return jet::EntityType::_Enemy;
}
