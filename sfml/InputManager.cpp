#include "InputManager.h"
#include <SFML/Window/Mouse.hpp>
#include <imgui.h>
#include "ImGuiUtility.h"


InputManager* g_inputManager;

InputManager::InputManager()
{
	m_keysMap[WalkLeft] = sf::Keyboard::A;
	m_keysMap[WalkRight] = sf::Keyboard::D;
	m_keysMap[Jump] = sf::Keyboard::Space;
	m_keysMap[LadderUp] = sf::Keyboard::W;
	m_keysMap[LadderDown] = sf::Keyboard::S;
	m_keysMap[Attack] = sf::Keyboard::F;
	initialize();
}

InputManager::~InputManager()
{
	m_keyActiveMap.clear();
	m_justPressedKeyMap.clear();
}

void InputManager::initialize()
{
	for(int i = START+1; i < LAST; ++i)
	{
		m_keyActiveMap.insert({ static_cast<Key>(i), false });
	}
	m_justPressedKeyMap = m_keyActiveMap;
}

void InputManager::update()
{
	m_isWindowFocused = m_window->hasFocus();

	for(auto&& k : m_keyActiveMap)
	{
		m_justPressedKeyMap[k.first] = (!m_keyActiveMap[k.first] && isKeyPressed(m_keysMap[k.first]));
		m_keyActiveMap[k.first] = isKeyPressed(m_keysMap[k.first]);
	}

	m_leftMouseJustPressed = (!m_leftMousePressed && isMouseButtonPressed(sf::Mouse::Button::Left));
	m_leftMousePressed = isMouseButtonPressed(sf::Mouse::Button::Left);
}

void InputManager::setWindow(sf::RenderWindow* window, sf::RenderTexture* renderTexture)
{
	m_window = window;
	m_renderTexture = renderTexture;
	m_windowScale.x = (window->getSize().x / static_cast<float>(1280));
	m_windowScale.y = (window->getSize().y / static_cast<float>(720));
}

bool InputManager::isKeyPressed(Key k)
{
	return (m_isWindowFocused && m_keyActiveMap[k]);
}

bool InputManager::isKeyJustPressed(Key k)
{
	return (m_isWindowFocused && m_justPressedKeyMap[k]);
}

bool InputManager::isKeyPressed(sf::Keyboard::Key k)
{
	return sf::Keyboard::isKeyPressed(k);
}

const sf::Vector2f InputManager::getMousePosition(const sf::View& view) const
{
	return m_window->mapPixelToCoords(sf::Mouse::getPosition(*m_window), view);
}

const sf::Vector2f InputManager::getMousePosition() const
{
	return InputManager::getMousePosition(m_window->getView());
}

const sf::Vector2f InputManager::getGameMousePosition() const
{
	return InputManager::getMousePosition(*m_gameView);
}

const sf::Vector2f InputManager::getGuiMousePosition() const
{
	return InputManager::getMousePosition(*m_guiView);
}


bool InputManager::isMouseButtonPressed(sf::Mouse::Button b) const
{
	return m_isWindowFocused && !ImGui::IsImGuiUsed() && sf::Mouse::isButtonPressed(b);
}

bool InputManager::isLeftMouseButtonJustPressed() const
{
	return m_isWindowFocused && m_leftMouseJustPressed;
}

void InputManager::setGameView(sf::View& gameView)
{
	m_gameView = &gameView;
}

void InputManager::setGuiView(sf::View& guiView)
{
	m_guiView = &guiView;
}
