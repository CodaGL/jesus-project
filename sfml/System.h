#pragma once
#include <typeindex>
#include <unordered_map>

namespace jet
{
	
	class EntityManager;

	//template<typename Derived>
	class System
	{
	public:
		System(EntityManager* entityManager)
			: m_entityManager(*entityManager)
		{}

		virtual ~System() = default;

		virtual void update() {}

	protected:

		EntityManager& m_entityManager;
	};
}