#pragma once
#include <string>
#include "json.hpp"

using json = nlohmann::json;

namespace jet
{
	template<typename T>
	inline T getJsonValue(json& json, const std::string& key, T defaultValue)
	{
		std::string modifiable = key;
		size_t pos = modifiable.find('/');
		if(pos != std::string::npos)
		{
			std::string code = modifiable.substr(0, pos);
			modifiable.erase(0, pos + 1);
			std::cout << "Code: " << code << std::endl;
			//Has /, so try to go
			return getJsonValue(json[code], modifiable, defaultValue);
		}
		return json.find(modifiable) == json.end() ? defaultValue : json[modifiable].get<T>();
	}
}
