#include "Animation.h"
#include <cassert>


jet::Animation::Animation(const sf::Time& frameTime, bool loop) :
	m_loop(loop),
	m_frameTime(frameTime)
{
}

void jet::Animation::addFrame(const sf::IntRect& bounds)
{
	m_frames.push_back(bounds);
}

const sf::IntRect& jet::Animation::getFrame(size_t index) const
{
	/*
	int frames = getFrameCount();
	if (frames == 1)
		return m_frames[0];
	if(!loop && !m_loop)
	{
		index = frames - 1;
	}
	else
	{
		index = index % (frames);
	}
	*/
	return m_frames[index];
}

bool jet::Animation::isLooped() const
{
	return m_loop;
}


size_t jet::Animation::getFrameCount() const
{
	return m_frames.size();
}

sf::Time jet::Animation::getFrameTime() const
{
	return m_frameTime;
}

sf::Time jet::Animation::getAnimationTime() const
{
	return sf::milliseconds(m_frameTime.asMilliseconds() * m_frames.size());
}

sf::Texture* jet::Animation::getTexture() const
{
	assert(m_texture != nullptr);
	return m_texture;
}

