#include "DebugRenderer.h"

#include <SFML/Graphics/View.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include "Game.h"

jet::DebugRenderer::DebugRenderer() :
	m_view(nullptr)
{

}

void jet::DebugRenderer::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.setView(*m_view);
	target.draw(m_tickText);
	target.draw(m_fpsText);
}

void jet::DebugRenderer::updateTicks()
{
	
}


void jet::DebugRenderer::updateFPS()
{
	m_frameCount++;

	if(m_fpsClock.getElapsedTime().asSeconds() > 0.5f)
	{
		m_fps = m_frameCount / m_fpsClock.restart().asSeconds();
		m_frameCount = 0;
		m_fpsText.setString("[WHITE]FPS: [GREEN]" + std::to_string(m_fps));
	}
	m_tickText.setString("[WHITE]Ticks: [GREEN]" + std::to_string(Game::lastTickCount) + "[WHITE]/s");
}


void jet::DebugRenderer::setView(sf::View& view)
{
	m_view = &view;
	m_tickText.setPosition(view.getSize().x - 192, 16);
	m_fpsText.setPosition(view.getSize().x - 192, 33);
}
