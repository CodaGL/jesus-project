#include "ScriptedBehavior.h"
#include "Mob.h"

#include <iostream>
#include "globals.h"
#include "Enemy.h"
#include "Math.h"


jet::ScriptedBehavior::ScriptedBehavior(const std::string& luaPath, Enemy* mob) :
	m_luaPath(luaPath),
	m_enemy(mob)
{
	//loadLuaScript();
	loadLuaScript();
}

void jet::ScriptedBehavior::tick()
{
	if (m_enemy == nullptr)
		return;
	/*Sqrat::Function tickFunction = Sqrat::RootTable().GetFunction("tick");
	if(!tickFunction.IsNull())
	{
		tickFunction.Execute(this);
	}*/
	
	try
	{
		g_vm->root_table().call<void>("tick", this);
	}
	catch (squall::squirrel_error& e)
	{
		std::cout << e.what() << std::endl;
	}
}

bool jet::ScriptedBehavior::loadLuaScript()
{
	squall::Klass<ScriptedBehavior> k(*g_vm, "Behavior");
	k.func("getPositionX", &ScriptedBehavior::getPositionX);
	k.func("setPositionX", &ScriptedBehavior::setPositionX);
	k.func("say", &ScriptedBehavior::say);
	k.func("getDistanceFromPlayer", &ScriptedBehavior::getDistanceFromPlayer);
	g_vm->dofile("res/scripts/test.nut");
	
	return true;
}


float jet::ScriptedBehavior::getPositionX() const
{
	return m_enemy->getCenterX();
}

float jet::ScriptedBehavior::getPositionY() const
{
	return m_enemy->getCenterY();
}

void jet::ScriptedBehavior::setPositionX(float x)
{
	m_enemy->setX(x);
}

void jet::ScriptedBehavior::setPositionY(float y)
{
	m_enemy->setY(y);
}

void jet::ScriptedBehavior::say(std::string word)
{
	std::cout << word << std::endl;
}

float jet::ScriptedBehavior::getDistanceFromPlayer()
{
	return Math::distance(m_enemy->getCenterX(), m_enemy->getCenterY(), m_enemy->getPlayer()->getCenterX(), m_enemy->getPlayer()->getCenterY());
}


void jet::ScriptedBehavior::reload()
{
	/*if (m_L != nullptr)
		luabridge::lua_close(m_L);
	loadLuaScript();*/
	
}