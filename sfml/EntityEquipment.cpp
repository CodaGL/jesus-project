#include "EntityEquipment.h"
#include "globals.h"
#include "ResourceManager.h"
#include "Mob.h"

jet::EntityEquipment::EntityEquipment(jet::Mob* owner, jet::ItemData& itemData) :
	m_owner(owner),
	m_itemData(itemData)
{
	loadAnimations();
	m_animatedSprite.setOrigin(48.0f / 2.0f, (48.0f + 16.0f) / 2.0f);
	setEntityState(m_owner->getEntityState());
}

void jet::EntityEquipment::update(const sf::Time& time)
{
	jet::AnimatedEntity::update(time);
}


void jet::EntityEquipment::tick()
{
	if(m_owner->getEntityState() != getEntityState())
	{
		setEntityState(m_owner->getEntityState(), true);
	}
	updatePosition();
}

void jet::EntityEquipment::updatePosition()
{
	setPosition(m_owner->getCenterX(), m_owner->getCenterY());
	m_animatedSprite.setFlippedX(m_owner->getDirection() == LEFT);
	if(m_owner->getAnimatedSprite().isPlaying())
	{
		m_animatedSprite.play();
	}
	else
	{
		m_animatedSprite.stop();
	}
}

const jet::ItemData& jet::EntityEquipment::getItemData() const
{
	return m_itemData;
}

jet::EntityType jet::EntityEquipment::getEntityType() const
{
	return jet::EntityType::_Equipment;
}


void jet::EntityEquipment::loadAnimations()
{
	if (!m_itemData.baseName.empty())
	{
		sf::Texture& texture = *g_resourceManager->getTexture("right_hand_" + m_itemData.baseName);

		int offset = 0;
		float frameSizeX = 48.0f;
		float frameSizeY = 48.0f;
		//Idle
		Animation* anim = new Animation();
		anim->setTexture(&texture);
		for (int i = 0; i < m_itemData.idleFrames; i++)
		{
			anim->addFrame(sf::IntRect(offset * frameSizeX, 0, frameSizeX, frameSizeY));
			offset++;
		}
		addAnimation(EntityState::Idle, anim);

		//Walk Anim
		anim = new Animation(sf::seconds(0.15f), true);
		anim->setTexture(&texture);
		for (int i = 0; i < m_itemData.walkingFrames; ++i)
		{
			anim->addFrame(sf::IntRect(offset * frameSizeX, 0, frameSizeX, frameSizeY));
			offset++;
		}
		addAnimation(EntityState::Moving, anim);

		//Jump Anim
		anim = new Animation();
		anim->setTexture(&texture);
		for (int i = 0; i < m_itemData.jumpingFrames; ++i)
		{
			anim->addFrame(sf::IntRect(offset * frameSizeX, 0, frameSizeX, frameSizeY));
			offset++;
		}
		addAnimation(EntityState::Jumping, anim);

		//In Air Anim
		anim = new Animation();
		anim->setTexture(&texture);
		for (int i = 0; i < m_itemData.inAirFrames; ++i)
		{
			anim->addFrame(sf::IntRect(offset * frameSizeX, 0, frameSizeX, frameSizeY));
			offset++;
		}
		addAnimation(EntityState::InAir, anim);

		/*OnLadder Idle Anim
		anim = new Animation();
		anim->setTexture(&texture);
		for (int i = 0; i < on_ladder_idle_frames; ++i)
		{
		anim->addFrame(sf::IntRect(offset * 32, 0, 32, 32));
		offset++;
		}
		addAnimation(EntityState::OnLadder_Idle, anim);*/

		//OnLadder Walking Anim
		anim = new Animation(sf::seconds(0.2f));
		anim->setTexture(&texture);
		for (int i = 0; i < m_itemData.ladderMovingFrames; ++i)
		{
			anim->addFrame(sf::IntRect(offset * frameSizeX, 0, frameSizeX, frameSizeY));
			offset++;
		}
		addAnimation(EntityState::OnLadder_Moving, anim);
		
		anim = new Animation(sf::seconds(0.1f));
		anim->setTexture(&texture);
		for (int i = 0; i < m_itemData.attackFrames; ++i)
		{
			anim->addFrame(sf::IntRect(offset * frameSizeX, 0, frameSizeX, frameSizeY));
			offset++;
		}
		addAnimation(EntityState::Attacking, anim);
	}
}
