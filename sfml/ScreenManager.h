#pragma once
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/System.hpp>

namespace jet
{
	class Screen;

	class ScreenManager
	{
	public:
		ScreenManager(Screen* initialScreen);
		~ScreenManager();

		void update(const sf::Time& time);
		void tick();

		void render(sf::RenderTarget& target);

		void resize(int width, int height);

		void setNextScreen(Screen* screen);

	private:
		Screen* m_currentScreen;
		Screen* m_nextScreen;
	};
}