#include "LineRectangle.h"
#include <SFML/Graphics/RenderTarget.hpp>

Utility::LineRectangle::LineRectangle(const sf::Vector2f& position, const sf::Vector2f& size, const std::vector<sf::Color>& colors)
	: m_vertices(sf::PrimitiveType::Lines, 8)
{
	setPosition(position);
	setScale(1.0f, 1.0f);
	m_size = size;
	for (int i = 0; i < colors.size(); ++i)
	{
		m_colors.push_back(colors[i]);
	}
	rebuild();
}

void Utility::LineRectangle::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	//states.transform *= getTransform();
	target.draw(m_vertices, states);
}

void Utility::LineRectangle::rebuild()
{
	m_vertices[0].position = { getPosition().x, getPosition().y };
	m_vertices[1].position = { getPosition().x + m_size.x, getPosition().y };

	m_vertices[2].position = { getPosition().x + m_size.x, getPosition().y };
	m_vertices[3].position = { getPosition().x + m_size.x, getPosition().y + m_size.y };

	m_vertices[4].position = { getPosition().x + m_size.x, getPosition().y + m_size.y };
	m_vertices[5].position = { getPosition().x, getPosition().y + m_size.y };

	m_vertices[6].position = { getPosition().x, getPosition().y + m_size.y };
	m_vertices[7].position = { getPosition().x, getPosition().y };

	int vcount = 0;
	for (int i = 0; i < m_colors.size(); ++i)
	{
		m_vertices[vcount].color = m_colors[i];
		m_vertices[vcount+1].color = m_colors[i];
		vcount += 2;
	}
}

void Utility::LineRectangle::setTickness(const sf::Vector2f& tickness)
{
	m_tickness = tickness;
}

const sf::Vector2f& Utility::LineRectangle::getTickness() const
{
	return m_tickness;
}

void Utility::LineRectangle::setSideColor(const RectangleSide& side, const sf::Color& color)
{
	m_colors[static_cast<uint8_t>(side)] = color;
	rebuild();
}