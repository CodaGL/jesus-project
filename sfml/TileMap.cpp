#include "TileMap.h"
#include "ResourceManager.h"
#include <SFML\Graphics.hpp>
#include <iostream>
#include "globals.h"

namespace jet
{
	bool TileMap::load(const std::string& tileSet, int width, int height, const std::vector<std::vector<TileData>>& layers)
	{

		m_texture = g_resourceManager->getTexture(tileSet);
		m_width = width;
		m_height = height;

		const int tilesNumber = m_texture->getSize().x / tileSize;
		//printf("%d\n", tilesNumber);
		for (size_t index = 0; index < layers.size(); ++index)
		{
			sf::VertexArray layer = sf::VertexArray();

			layer.setPrimitiveType(sf::PrimitiveType::Quads);
			layer.resize(m_width * m_height * 4);

			for (uint16_t y = 0; y < m_height; ++y)
			{
				for (uint16_t x = 0; x < m_width; ++x)
				{
					TileData tileData = layers[index][x + y * m_width];
					if (tileData == 0)
						continue;
					tileData--;
					//printf("%d\n", tileData);
					const int tu = tileData % tilesNumber;
					const int tv = tileData / tilesNumber;

					sf::Vertex* quad = &layer[(x + y * width) * 4];

					quad[0].position = sf::Vector2f(x * tileSize, y * tileSize);
					quad[1].position = sf::Vector2f((x + 1) * tileSize, y * tileSize);
					quad[2].position = sf::Vector2f((x + 1) * tileSize, (y + 1) * tileSize);
					quad[3].position = sf::Vector2f(x * tileSize, (y + 1) * tileSize);

					quad[0].texCoords = sf::Vector2f(tu * tileSize, tv * tileSize);
					quad[1].texCoords = sf::Vector2f((tu + 1) * tileSize, tv * tileSize);
					quad[2].texCoords = sf::Vector2f((tu + 1) * tileSize, (tv + 1) * tileSize);
					quad[3].texCoords = sf::Vector2f(tu * tileSize, (tv + 1) * tileSize);
				}
			}
			m_layers.push_back(layer);
		}
		return true;
	}

	void TileMap::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		states.transform *= getTransform();
		states.texture = m_texture;
		for (auto&& layer : m_layers)
		{
			target.draw(layer, states);
		}
	}

}
