#include "LightEntity.h"
#include "globals.h"

jet::LightEntity::LightEntity(LightData& data) :
	m_lightData(data)
{
}


void jet::LightEntity::init()
{
	sf::Vector2u sz = g_resourceManager->getTexture("lightMap")->getSize();
	m_sprite.setTexture(*g_resourceManager->getTexture("lightMap"));
	m_sprite.setOrigin(sz.x / 2.0f, sz.y / 2.0f);
	
	m_sprite.setColor(sf::Color::White);
	m_sprite.setPosition(m_lightData.position);
	
	m_animationTime += static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
	
	float scaleX = m_lightData.radius.x + AMPLITUDE * sin(FREQUENCY * m_animationTime);
	float scaleY = m_lightData.radius.y + AMPLITUDE * sin(FREQUENCY * m_animationTime);

	m_sprite.setScale(scaleX/64.0f, scaleY/64.0f);
}

void jet::LightEntity::update(const sf::Time& time)
{
	m_animationTime += time.asSeconds();

	const float scaleX = m_lightData.radius.x + AMPLITUDE * sin(FREQUENCY * m_animationTime);
	const float scaleY = m_lightData.radius.y + AMPLITUDE * sin(FREQUENCY * m_animationTime);

	m_sprite.setScale(scaleX, scaleY);
}

void jet::LightEntity::render(sf::RenderTarget& target) const
{
	sf::RenderStates states;
	states.blendMode = sf::BlendAdd;
	target.draw(m_sprite, states);
}

jet::LightData& jet::LightEntity::getLightData()
{
	return m_lightData;
}

jet::EntityType jet::LightEntity::getEntityType() const
{
	return jet::EntityType::_Light;
}
