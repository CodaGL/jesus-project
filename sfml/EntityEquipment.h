#pragma once
#include "AnimatedEntity.h"
#include "ItemData.h"

namespace jet
{

	enum class EquipmentType
	{
		Head,
		Body,
		LeftArm,
		RightArm,
		LeftHand,
		RightHand,
		Legs,
		Boots,
	};
	
	class Mob;

	class EntityEquipment : public AnimatedEntity
	{
	public:
		EntityEquipment() = default;
		EntityEquipment(jet::Mob* owner, jet::ItemData& itemData);

		void update(const sf::Time& time) override;

		void tick() override;

		void updatePosition();

		const jet::ItemData& getItemData() const;
	
		jet::EntityType getEntityType() const override;

	private:
		jet::Mob* m_owner;
		jet::ItemData m_itemData;

		void loadAnimations();
	};
}
