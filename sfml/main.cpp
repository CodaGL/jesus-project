#include "globals.h"
#include "Game.h"

#include "InputManager.h"
#include "ResourceManager.h"

#include <iostream>
#include <squall_vmstd.hpp>

squall::VMStd* g_vm;


int main()
{
	srand(time_t(NULL));

	/*HSQUIRRELVM vm;
	Sqrat::DefaultVM::Set(vm);
	std::cout << Sqrat::Error::Message(vm) << std::endl;*/

	g_vm = new squall::VMStd(1024);
	g_inputManager = new InputManager();
	g_resourceManager = new jet::ResourceManager();

	jet::Game game;
	game.start();
	
	//sq_close(vm);

	delete g_inputManager;
	delete g_resourceManager;
	return 0;
}