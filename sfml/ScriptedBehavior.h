#pragma once
#include <string>

#include <squall_klass.hpp>
#include <squall_table.hpp>


namespace jet
{
	class Enemy;
	class Mob;

	class ScriptedBehavior
	{

	public:
		ScriptedBehavior(const std::string& luaPath, Enemy* mob);

		void tick();

		float getPositionX() const;
		float getPositionY() const;
		void setPositionX(float x);
		void setPositionY(float y);
		void say(std::string word);

		float getDistanceFromPlayer();

		void reload();

	private:

		std::string m_luaPath;

		Enemy* m_enemy;

		bool loadLuaScript();
	};

	
}
