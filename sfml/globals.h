#pragma once

#include <SFML\System\Vector2.hpp>
#include <squall_vmstd.hpp>
#include "ResourceManager.h"

class InputManager;

namespace jet {
	class ResourceManager;
}

extern InputManager* g_inputManager;
extern jet::ResourceManager* g_resourceManager;
extern squall::VMStd* g_vm;

inline sf::Vector2f pixelToWorld(int x, int y)
{
	static const float pixelsPerUnit = 32.0f;
	return sf::Vector2f(x / pixelsPerUnit, y / pixelsPerUnit);
}

inline sf::Vector2f worldToPixel(float x, float y)
{
	static const float pixelsPerUnit = 32.0f;
	return sf::Vector2f(x * pixelsPerUnit, y * pixelsPerUnit);
}

