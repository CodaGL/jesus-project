#include "NinePatch.h"

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Texture.hpp>

jet::NinePatch::NinePatch(sf::Texture* texture, int left, int top, int right, int bot) :
	m_texture(texture),
	m_vertices(sf::PrimitiveType::Quads, 9 * 4),
	m_patchSize(left, top)
{
	int middleWidth = texture->getSize().x - left - right;
	int middleHeight = texture->getSize().y - top - bot;
	m_patches.resize(9);
	m_patches[jet::NinePatchIndex::_TopLeft] = sf::IntRect(0, 0, left, top);
	m_patches[jet::NinePatchIndex::_TopCenter] = sf::IntRect(left, 0, middleWidth, top);
	m_patches[jet::NinePatchIndex::_TopRight] = sf::IntRect(left + middleWidth, 0, right, top);

	m_patches[jet::NinePatchIndex::_CenterLeft] = sf::IntRect(0, top, left, middleHeight);
	m_patches[jet::NinePatchIndex::_CenterCenter] = sf::IntRect(left, top, middleWidth, middleHeight);
	m_patches[jet::NinePatchIndex::_CenterRight] = sf::IntRect(left + middleWidth, top, right, middleHeight);

	m_patches[jet::NinePatchIndex::_BotLeft] = sf::IntRect(0, top + middleHeight, left, top);
	m_patches[jet::NinePatchIndex::_BotCenter] = sf::IntRect(left, top + middleHeight, middleWidth, bot);
	m_patches[jet::NinePatchIndex::_BotRight] = sf::IntRect(left + middleWidth, top + middleHeight, right, bot);

	
}

void jet::NinePatch::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	states.texture = m_texture;

	target.draw(m_vertices, states);
}

void jet::NinePatch::rebuildVertices()
{
	m_vertices.clear();
	m_vertices.resize(9 * 4);
	
	set(_TopLeft, 0, 0, m_patchSize.x, m_patchSize.y);
	set(_TopCenter, m_patchSize.x, 0, m_size.x - m_patchSize.x, m_patchSize.y);
	set(_TopRight, m_size.x - m_patchSize.x, 0, m_patchSize.x, m_patchSize.y);
	
	set(_CenterLeft, 0, m_patchSize.y, m_patchSize.x, m_size.y - m_patchSize.y);
	set(_CenterCenter, m_patchSize.x, m_patchSize.y, m_size.x - m_patchSize.x, m_size.y - m_patchSize.y);
	set(_CenterRight, m_size.x - m_patchSize.x, m_patchSize.y, m_patchSize.x, m_size.y - m_patchSize.y);
	
	set(_BotLeft, 0, m_size.y - m_patchSize.y, m_patchSize.x, m_patchSize.y);
	set(_BotCenter, m_patchSize.x, m_size.y - m_patchSize.y, m_size.x - m_patchSize.x, m_patchSize.y);
	set(_BotRight, m_size.x - m_patchSize.x, m_size.y - m_patchSize.y, m_patchSize.x, m_patchSize.y);
	
	for(int i = 0; i < _PatchMax; ++i)
	{
		add(static_cast<NinePatchIndex>(i), m_patches[i]);
	}
}

void jet::NinePatch::add(jet::NinePatchIndex idx, const sf::IntRect& r)
{
	if (idx <= jet::NinePatchIndex::_UndefinedPatch)
		return;
	const float left = static_cast<float>(r.left);
	const float width = static_cast<float>(r.width);
	const float top = static_cast<float>(r.top);
	const float height = static_cast<float>(r.height);
	sf::Vertex* quad = &m_vertices[idx * 4];
	quad[0].texCoords = { left, top };
	quad[1].texCoords = { left + width, top };
	quad[2].texCoords = { left + width, top + height};
	quad[3].texCoords = { left, top + height};
}


void jet::NinePatch::set(jet::NinePatchIndex index, float x, float y, float width, float height)
{
	if (index <= jet::NinePatchIndex::_UndefinedPatch)
		return;
	const float dx = x + width;
	const float dy = y + height;
	sf::Vertex* quad = &m_vertices[index*4];
	quad[0].position = { x, y };
	quad[1].position = { dx, y };
	quad[2].position = { dx, dy };
	quad[3].position = { x, dy };
}


void jet::NinePatch::setTexture(sf::Texture* texture)
{
	m_texture = texture;
}

sf::Texture* jet::NinePatch::getTexture() const
{
	return m_texture;
}

void jet::NinePatch::setSize(const sf::Vector2f& size)
{
	m_size = size;
	rebuildVertices();
}

const sf::Vector2f& jet::NinePatch::getSize() const
{
	return m_size;
}