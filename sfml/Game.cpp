#include "Game.h"

#include <iostream>

#include "globals.h"
#include "TileMap.h"

#include "imgui.h"
#include "imgui-SFML.h"
#include "LevelScreen.h"
#include "InputManager.h"
#include "LevelEditorScreen.h"

namespace jet
{
	int Game::lastTickCount = 0;

	Game::Game()
	{
		m_renderTexture.create(1280, 720);
		initializeWindow();

		m_mainSprite.setTexture(m_renderTexture.getTexture());

		m_screenManager = new ScreenManager(new LevelScreen());

	}

	Game::~Game()
	{
		delete m_screenManager;
	}


	void Game::start()
	{

		ImGui::SFML::Init(m_window);

		g_inputManager->setWindow(&m_window, &m_renderTexture);


		//GAME LOOP
		const float TICK_RATE = 1.0f / 60.0f;
		float accumulator = 0.0f;
		float remainder = 0.0f;
		float lastTime = 0.0f;
	
		int ticks = 0;
		static int lastTicks = 0;

		sf::Clock clock;
		while (m_window.isOpen())
		{
			sf::Event event;
			while (m_window.pollEvent(event))
			{
				ImGui::SFML::ProcessEvent(event);
				handleEvent(event);
			}
			const sf::Time time = clock.restart();
			ImGui::SFML::Update(m_window, time);
			
			g_inputManager->update();
			
			m_screenManager->update(time);

			//GAME LOOP
			float passed = time.asSeconds();
			accumulator += passed;
			passed += remainder;

			int nticks = static_cast<int>((passed / TICK_RATE));
			remainder = passed - nticks * TICK_RATE;
			ticks += nticks;
			while (nticks > 0)
			{
				this->tick();
				nticks--;
			}
			//END GAME LOOP

			if (ImGui::Begin("Engine (DEBUG)"))
			{
				char chr[20];
				sprintf_s(chr, "Ticks: %d/s", lastTicks);
				ImGui::Text(chr);
			}
			ImGui::End();

			render();


			if (accumulator >= 1.0f) {
				//std::cout << "Ticks: " << ticks << std::endl;
				lastTicks = ticks;
				Game::lastTickCount = lastTicks;
				ticks = 0;
				accumulator -= 1.0f;
			}

		}
		ImGui::SFML::Shutdown();
	}

	void Game::render()
	{
		m_window.clear(sf::Color::Red);
		m_renderTexture.clear();

		m_screenManager->render(m_renderTexture);

		m_renderTexture.display();
		m_window.draw(m_mainSprite);

		ImGui::SFML::Render(m_window);

		m_window.display();
	}

	void Game::tick()
	{
		m_screenManager->tick();
	}

	void Game::handleEvent(sf::Event& event)
	{
		if (event.type == sf::Event::Closed)
		{
			m_window.close();
		}
		else if (event.type == sf::Event::Resized)
		{
			sf::Event::SizeEvent& size = event.size;
			if (size.width < 800)
				size.width = 800;
			if (size.height < 600)
				size.height = 600;

			m_screenManager->resize(event.size.width, event.size.height);
			m_window.setSize(sf::Vector2u(size.width, size.height));
		}
	}

	void Game::initializeWindow()
	{
		auto const windowMode = sf::VideoMode(1280, 720);
		int desiredWidth = 1280;
		int desiredHeight = 720;
		sf::Vector2f spriteScale = sf::Vector2f(
			windowMode.width / desiredWidth,
			windowMode.height / desiredHeight);

		m_window.create(windowMode, "Another Useless Prototype");
		m_window.setFramerateLimit(60);

		m_mainSprite.setScale(spriteScale);
	}
}
