#pragma once
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include "Widget.h"

namespace jet
{
	enum NinePatchIndex
	{
		_UndefinedPatch = -1,
		//Top Row
		_TopLeft = 0,
		_TopCenter = 1,
		_TopRight = 2,
		//Center Row
		_CenterLeft = 3,
		_CenterCenter = 4,
		_CenterRight = 5,
		//Bottom Row
		_BotLeft = 6,
		_BotCenter = 7,
		_BotRight = 8,

		_PatchMax
	};

	class NinePatch : public virtual Widget//public virtual sf::Drawable, public virtual sf::Transformable
	{
		
	public:
		NinePatch(sf::Texture* texture, int left = 0, int top = 0, int right = 0, int bot = 0);

		void setTexture(sf::Texture*);
		sf::Texture* getTexture() const;

		void setSize(const sf::Vector2f& size);
		const sf::Vector2f& getSize() const;

	private:
		sf::Texture* m_texture;

		sf::VertexArray m_vertices;

		std::vector<sf::IntRect> m_patches;

		sf::Vector2f m_size;
		sf::Vector2f m_patchSize;

	private:
		void rebuildVertices();
		
		void add(jet::NinePatchIndex idx, const sf::IntRect&);
		void set(jet::NinePatchIndex index, float x, float y, float width, float height);

		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
		
		void update(const sf::Time& time) override{}
	};
}
