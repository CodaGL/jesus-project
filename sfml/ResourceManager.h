#pragma once
#include "Singleton.h"
#include <SFML\Graphics\Texture.hpp>
#include <map>
#include "BitmapFont.h"
#include "ItemData.h"

namespace jet
{
	class ResourceManager
	{
	public:
		ResourceManager();
		~ResourceManager();

		void init();

		sf::Texture* getTexture(const std::string& name);
		BitmapFont* getBitmapFont(const std::string& name);
		jet::ItemData* getWeapon(const std::string& name);

		void loadTexture(const std::string& name, const std::string& path);
		void loadBitmapFont(const std::string& name, const std::string& path);
		void loadWeapon(const std::string& name, const std::string& path);

		template<typename T>
		bool loadResource(std::map<std::string, T*>& container, const std::string& path, const std::string& key);

	private:
		std::map<std::string, sf::Texture*> m_textures;
		std::map<std::string, jet::BitmapFont*> m_bitmapFonts;
		std::map<std::string, jet::ItemData*> m_weapons;
	};
}
