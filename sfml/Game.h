#pragma once
#include <SFML\Graphics.hpp>
#include <SFML\Graphics\RenderTarget.hpp>
#include "ScreenManager.h"

namespace jet
{
	class Game
	{
	public:
		static int lastTickCount;

	public:
		Game();
		~Game();

		void start();

		void tick();

		void render();

		void handleEvent(sf::Event& event);
		
		void initializeWindow();
	private:

		sf::RenderWindow m_window;

		sf::Sprite m_mainSprite;
		sf::RenderTexture m_renderTexture;

		ScreenManager* m_screenManager;
	};
}
