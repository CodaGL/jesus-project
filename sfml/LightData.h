#pragma once
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Color.hpp>

namespace jet
{
	struct LightData
	{
		sf::Vector2f position;
		sf::Vector2f radius;
		sf::Color color;
		
		LightData() = default;

		LightData(const sf::Vector2f& pos, const sf::Vector2f& rad, const sf::Color& col) :
			position(pos),
			radius(rad),
			color(col)
		{

		}
	};
}