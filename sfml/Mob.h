#pragma once

#include "AnimatedEntity.h"

#include <SFML/System/Vector2.hpp>
#include "ScriptedBehavior.h"

namespace jet
{
	enum Direction : uint8_t
	{
		LEFT,
		RIGHT
	};

	class Mob : public AnimatedEntity
	{
	protected:
		ScriptedBehavior* m_scriptedBehavior;

	protected:
		sf::Vector2f m_velocity;

		Direction m_direction;

		bool m_applyGravity;

		int m_health;
		int m_maxHealth;

		int m_knockBackTicks;
		sf::Vector2f m_knockBackVelocity;

		virtual void onDie(Entity* killerId){};

		virtual void loadAnimations(const std::string& name, float frameSizeX, float frameSizeY);

	public:
		~Mob()
		{
			delete m_scriptedBehavior;
		}
		void processCollisions() override;

		virtual void render(sf::RenderTarget& target) const;

		void tick() override;
		void postTick() override;

		bool isGrounded() const;
		bool isCeiled() const;

		const sf::Vector2f& getVelocity() const;

		void setVelocity(const float x, const float y);
		void setVelocity(const sf::Vector2f& velocity);

		void setHealth(int health);
		
		int getHealth() const;
		int getMaxHealth() const;

		virtual void damage(int health, Entity* damagedBy);

		virtual void setKnockback(float dx, float dy, int time)
		{
			if (time <= 0)
				return;
			m_knockBackTicks = time;
			m_knockBackVelocity.x = dx;
			m_knockBackVelocity.y = dy;
		}

		Direction getDirection() const;

	};
}
