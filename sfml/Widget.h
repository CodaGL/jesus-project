#pragma once
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include "BitmapText.h"

namespace sf {
	class Time;
}

namespace jet
{
	class Widget : public virtual sf::Drawable, public virtual sf::Transformable
	{
	public:
		Widget();
		virtual ~Widget() = default;

		virtual void update(const sf::Time& time);

		void setParent(Widget* widget);
		Widget* getParent() const;

		/* Gets the Transform relatrive to the parent (if any)*/
		const sf::Transform& getRelative() const;

		void setPosition(const sf::Vector2f& pos);

		float getWidth() const
		{
			return m_width;
		}

		float getHeight() const
		{
			return m_height;
		}
	protected:
		float m_width;
		float m_height;

	private:

		Widget* m_parent;


		bool m_isContained;

		sf::RectangleShape m_shape;
		BitmapText m_debugText;

		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	};
}
