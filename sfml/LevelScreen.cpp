#include "LevelScreen.h"
#include <iostream>
#include "TileMap.h"
#include "ResourceManager.h"
#include <imgui.h>
#include "LevelEditorScreen.h"
#include "LevelReader.h"
#include "globals.h"
#include "InputManager.h"
#include "Skeleton.h"
#include "Math.h"
#include "LevelRenderer.h"
#include "ObjectFactory.h"
#include "LevelLoader.h"
#include "EntityEquipment.h"
#include "AnimatedEntity.h"
#include "BitmapFont.h"
#include "BitmapText.h"

jet::LevelScreen::LevelScreen()
		
//m_view(sf::Vector2f(1280.0f / 2.0f, 720.0f / 2.0f), sf::Vector2f(1280.0f, 720.0f))
{
	m_view = sf::View(worldToPixel(15.0f, 10.0f) / 2.0f, worldToPixel(15.0f, 10.0f)); //sf::Vector2f(15.0f/2.0f, 10.0f/2.0f), sf::Vector2f(15, 10));
	m_view.move(0.0f, 0.0f);

	sf::Vector2f sz = worldToPixel(15.0f*2.0f, 10.0f*2.0f);
	m_guiView = sf::View(sz / 2.0f, sz); //sf::Vector2f(15.0f/2.0f, 10.0f/2.0f), sf::Vector2f(15, 10));

	m_debugRenderer = new DebugRenderer();
	m_debugRenderer->setView(m_guiView);
}

jet::LevelScreen::~LevelScreen()
{
	delete m_level;
	delete m_player;
}


void jet::LevelScreen::onEnter()
{
	std::cout << "Level Screen: onEnter" << std::endl;

	m_player = new jet::Player();
	m_player->init();

	g_inputManager->setGameView(m_view);
	g_inputManager->setGuiView(m_guiView);


	switchLevel("index_map");

	m_bitmapText.setPosition(100, 300);
	m_bitmapText.setString(" !\"#$%&'()*+,-./0123456789:;<=>[#00FF00FF]?@abcdefghijklmnopqrstuvwxyz[\\]^_\n !\"#$%&'()*+,-./0123456789:;<=>\t?@abcdefghijklmnopqrstuvwxyz[\\]^_");
	//m_bitmapText.setColor(sf::Color::Red);

	Widget* widget = new Widget();
	widget->setPosition({ 500, 300 });
	//m_gui.add(widget);

	Widget* wtest = new Widget();
	wtest->setParent(widget);
	wtest->setPosition({ 0, 0 });
	m_mouseFollower = wtest;

	//m_gui.add(wtest);
	m_ninePatch = new NinePatch(g_resourceManager->getTexture("ninepatch_1"), 12, 12, 12, 12);
	m_ninePatch->setPosition({ 100, 100 });
	m_ninePatch->setSize({ 100, 100 });
	//m_gui.add(m_ninePatch);
}

void jet::LevelScreen::onExit()
{
	std::cout << "Level Screen: onExit" << std::endl;

}

void jet::LevelScreen::render(sf::RenderTarget& target)
{
	target.clear(sf::Color(135, 206, 250));
		
	target.setView(m_view);
		
	if(m_levelRenderer != nullptr)
	{
		m_levelRenderer->renderBackground(target, sf::RenderStates::Default);
		m_levelRenderer->renderMiddleground(target, sf::RenderStates::Default);
		m_levelRenderer->renderObjects(jet::EntityType::_Enemy, target, sf::RenderStates::Default);
		if (m_player->getDirection() == RIGHT)
		{
			m_levelRenderer->renderObjects(jet::EntityType::_Player, target, sf::RenderStates::Default);
			m_levelRenderer->renderObjects(jet::EntityType::_Equipment, target, sf::RenderStates::Default);
		}
		else if (m_player->getDirection() == LEFT)
		{
			m_levelRenderer->renderObjects(jet::EntityType::_Equipment, target, sf::RenderStates::Default);
			m_levelRenderer->renderObjects(jet::EntityType::_Player, target, sf::RenderStates::Default);
		}
		m_levelRenderer->renderObjects(jet::EntityType::_Damage, target, sf::RenderStates::Default);
		//m_levelRenderer->renderObjects(target, sf::RenderStates::Default);
		m_levelRenderer->renderForeground(target, sf::RenderStates::Default);

		//m_levelRenderer->renderLights(target);
	}
}

void jet::LevelScreen::renderUI(sf::RenderTarget& target)
{
	target.setView(m_guiView);
	m_player->renderGUI();

	if(m_debugRenderer != nullptr)
	{
		m_debugRenderer->updateFPS();
		target.draw(*m_debugRenderer);
	}

	target.draw(m_bitmapText);

	target.draw(m_gui);

	sf::Sprite sprite;
	sf::Texture* texture = g_resourceManager->getTexture("heart");
	texture->setSmooth(false);
	sprite.setTexture(*texture);

	if (m_level != nullptr && m_player != nullptr)
	{
		const float hearts_x_start = m_guiView.getSize().x / 2.0f - (m_player->getMaxHealth()*32.0f) / 2;
		const float hearts_y_start = 32.0f;
		for (int i = 0; i < m_player->getHealth(); ++i)
		{
			sprite.setPosition(hearts_x_start + i * 32.0f + 2.0f, hearts_y_start);
			target.draw(sprite);
		}
	}

	if (ImGui::Begin("Funcs"))
	{
		static float zoom;
		if (ImGui::Button("Zoom Out"))
		{
			zoom *= 1.1f;
			m_view.zoom(1.1f);
		}
		if (ImGui::Button("Zoom In"))
		{
			zoom *= 0.9f;
			m_view.zoom(0.9f);
		}
		static char bf[255];
		ImGui::InputText("Test String", bf, 255);
		std::string wrap = bf;
		if(m_bitmapText.getString() != wrap)
			m_bitmapText.setString(bf);
	}
	ImGui::End();

	Screen::renderUI(target);
}

void jet::LevelScreen::update(const sf::Time& time)
{
	if (m_level != nullptr)
	{
		m_level->update(time);
	}

	m_gui.update(time);
	//std::cout << g_inputManager->getGuiMousePosition().x << std::endl;
	m_mouseFollower->setPosition(g_inputManager->getGuiMousePosition());
}


int ticks = 0;

void jet::LevelScreen::tick()
{
	ticks++;
	if (g_inputManager->isLeftMouseButtonJustPressed())
	{
		const sf::Vector2f pos = g_inputManager->getGameMousePosition();
		//m_levelData.lights.push_back({ pos,{ m_lightSizeX, m_lightSizeY }, { m_redLightColor, m_greenLightColor, m_blueLightColor } });

	}
	if(m_level != nullptr)
	{
		m_level->tick();
		m_level->postTick();
		m_level->processCollisions();
	}

	if (m_player != nullptr)
	{
		sf::Vector2f v = { m_player->getX(), m_player->getY() };
		//Keeps the view in level bounds.
		v.x = Math::clamp(v.x, m_view.getSize().x / 2.0f, m_levelData.width * 32.0f - m_view.getSize().x / 2.0f);
		v.y = Math::clamp(v.y, m_view.getSize().y / 2.0f, m_levelData.height * 32.0f - m_view.getSize().y / 2.0f);
		m_view.setCenter(v);
	}

}

void jet::LevelScreen::resize(int width, int height)
{
	m_guiView.setCenter(sf::Vector2f(width / 2.0f, height / 2.0f));
	m_guiView.setSize(sf::Vector2f(static_cast<float>(width), static_cast<float>(height)));
	m_debugRenderer->setView(m_guiView);
}

bool jet::LevelScreen::switchLevel(const std::string& name)
{
	if (name.empty())
		return false;
	if(m_level != nullptr)
	{
		std::cout << "Deleting old level" << std::endl;
		delete m_level;
	}

	LevelReader reader;
	
	if(!reader.readLevel("res/levels/" + name + "/" + name + ".tmx", m_levelData))
	{
		std::cout << "Unable to load level: " << name << std::endl;
		return false;
	}
	
	m_level = new Level(m_levelData, *m_player);

	LevelLoader loader;
	loader.loadObjects(m_levelData, *m_level);

	delete m_levelRenderer;
	m_levelRenderer = new LevelRenderer(this, *m_level, m_levelData);

	m_player->setLevel(m_level);

	jet::EntityEquipment* equipment = new jet::EntityEquipment(m_player, *g_resourceManager->getWeapon("iron_sword"));
	m_level->add(equipment);

	std::cout << "Switching Level..." << std::endl;
	return true;
}

LevelData& jet::LevelScreen::getLevelData()
{
	return m_levelData;
}
