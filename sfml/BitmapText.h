#pragma once
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include "BitmapFont.h"

namespace jet
{
	class BitmapText final : public virtual sf::Drawable, public virtual sf::Transformable
	{
	public:

	
	public:
		BitmapText();
		
		void rebuild();
		
		void setColor(const sf::Color& color);
		const sf::Color& getColor() const;

		void setString(const std::string& string);
		const std::string& getString() const;

	private:
		sf::VertexArray m_vertices;
		
		BitmapFont* m_font;

		sf::Color m_color;
		sf::Color m_lastMarkupColor;
		std::string m_string;

	private:
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

		sf::Color getColorFromName(const std::string& name);

		//void addGlyph(float x, float y, char character, float characterSize);

		//Returns the number of characters to skip
		int parseMarkupLanguage(const std::string&, sf::Color& currentColor, int start, int end);
	};
}
