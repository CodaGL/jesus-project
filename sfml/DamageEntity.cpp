#include "DamageEntity.h"
#include "Mob.h"
#include "Enemy.h"

jet::DamageEntity::DamageEntity(const Entity* source, float rangeX, float rangeY, int time, int damage) :
	m_source(source),
	m_timeTicks(time),
	m_damage(damage)
{
	m_isAlive = true;
	m_isCollidableX = m_isCollidableY = true;
}


void jet::DamageEntity::tick()
{
	if (!m_isAlive)
		return;
	std::cout << "DAMAGE" << std::endl;
	m_timeTicks--;
	if (m_timeTicks <= 0)
	{
		m_isAlive = false;
		applyDamage();
		return;
	}
}

void jet::DamageEntity::applyDamage()
{
	for(auto&& entity : m_damagedEntities)
	{
		if (entity->getEntityType() == EntityType::_Enemy)
		{
			jet::Enemy* enemy = dynamic_cast<jet::Enemy*>(entity);
			enemy->damage(m_damage, this);
			if(m_source != nullptr)
			{
				if(m_source->getEntityType() == _Enemy || m_source->getEntityType() == _Player)
				{
					const jet::Mob* mob = dynamic_cast<const jet::Mob*>(m_source);
					enemy->setKnockback(mob->getDirection() == RIGHT ? 1 : -1, -1, 24);
				}
			}
		}
	}
}


void jet::DamageEntity::onCollision(Entity* entity, ECollisionFlags collisionFlag)
{
	if (entity == m_source)
		return;
	m_damagedEntities.insert(entity);
}

jet::EntityType jet::DamageEntity::getEntityType() const
{
	return _Damage;
}
