#pragma once
#include <SFML/Graphics/Texture.hpp>
#include <SFML/System/Time.hpp>


namespace jet
{
	class Animation
	{
	public:
		explicit Animation(const sf::Time& frameTime = sf::milliseconds(100), bool loop = true);

		~Animation() = default;
	
		void addFrame(const sf::IntRect& bounds);
		const sf::IntRect& getFrame(size_t index/*, bool loop = false*/) const;

		void setTexture(sf::Texture* texture)
		{
			m_texture = texture;
		}

		bool isLooped() const;

		size_t getFrameCount() const;
		
		sf::Time getFrameTime() const;
		sf::Time getAnimationTime() const;

		sf::Texture* getTexture() const;

	private:
		sf::Texture* m_texture;

		std::vector<sf::IntRect> m_frames;

		bool m_loop;

		sf::Time m_frameTime;
	};

}
