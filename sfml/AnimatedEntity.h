#pragma once
#include <string>
#include <map>
#include "Animation.h"
#include "Entity.h"
#include "AnimatedSprite.h"

namespace jet
{
	class AnimatedEntity : public virtual Entity
	{
	public:
		AnimatedEntity();
		virtual ~AnimatedEntity();

		virtual void render(sf::RenderTarget& target) const override;

		virtual void update(const sf::Time& time) override;

		void setAnimation(const Animation* animation);
		void addAnimation(EntityState entityState, jet::Animation* anim);
		const Animation* getAnimation(EntityState entityState) const;

		void setEntityState(EntityState e, bool updateAnimation);
		void setEntityState(EntityState e) override;

		virtual void setPosition(float x, float y) override;

		void setSpriteColor(const sf::Color& color, const sf::Time& time);
		const sf::Color& getSpriteColor() const;

		//Returns true if the state is playing (currentState == state and isPlaying() == true)
		bool isPlaying(EntityState state) const;

		size_t getAnimationFrame() const;

		jet::AnimatedSprite& getAnimatedSprite()
		{
			return m_animatedSprite;
		}

		EntityState getCurrentAnimationEntityState()
		{
			return m_currentAnimationEntityState;
		}

	protected:
		jet::AnimatedSprite m_animatedSprite;

		sf::Time m_coloredTime;
		sf::Color m_currentSpriteColor;

	private:

		std::map<EntityState, jet::Animation*> m_animations;
		
		EntityState m_currentAnimationEntityState;
	};

}
