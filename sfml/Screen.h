#pragma once
#include <SFML/Graphics/RenderTarget.hpp>
#include <vector>
#include "BitmapText.h"

namespace sf {
	class Time;
}

namespace jet
{
	class ScreenManager;
	
	class Screen
	{
	public:

		virtual ~Screen() = default;

		virtual void onEnter() = 0;
		virtual void onExit() = 0;

		virtual void update(const sf::Time& time) = 0;
		virtual void tick() = 0;

		virtual void render(sf::RenderTarget& target) = 0;
		virtual void renderUI(sf::RenderTarget& target){}

		virtual void resize(int width, int height) {};

		void setNextScreen(Screen* screen);

		void setScreenManager(ScreenManager* screenManager);

	private:

			ScreenManager* m_screenManager;

			BitmapText m_debugText;
	};
}
