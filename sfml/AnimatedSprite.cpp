#include "AnimatedSprite.h"
#include "Animation.h"

#include <cassert>

#include <SFML/System/Clock.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <iostream>

jet::AnimatedSprite::AnimatedSprite() :
	m_currentAnimation(nullptr),
	m_isLoop(false),
	m_isPaused(true),
	m_vertices(sf::PrimitiveType::Quads, 4)
{
	
}

void jet::AnimatedSprite::setAnimation(const Animation* animation)
{
	assert(animation != nullptr);
	m_currentAnimation = animation;
	m_texture = m_currentAnimation->getTexture();
	m_isLoop = m_currentAnimation->isLooped();
	m_frameTime = m_currentAnimation->getFrameTime();
	m_currentTime = sf::milliseconds(0);
	m_currentFrame = 0;
	setFrame(m_currentFrame);
	play();
}

void jet::AnimatedSprite::play()
{
	m_isPaused = false;
}

void jet::AnimatedSprite::play(Animation* animation)
{
	if (m_currentAnimation != animation)
		setAnimation(animation);
	play();
}

void jet::AnimatedSprite::pause()
{
	m_isPaused = true;
}

void jet::AnimatedSprite::stop()
{
	m_isPaused = true;
	m_currentFrame = 0;
	setFrame(m_currentFrame);
}

void jet::AnimatedSprite::setFrame(size_t id)
{
	if(m_currentAnimation != nullptr)
	{
		sf::IntRect frame = m_currentAnimation->getFrame(m_currentFrame);
		m_vertices[0].position = { 0.0f, 0.0f };
		m_vertices[1].position = { static_cast<float>(frame.width), 0.0f };
		m_vertices[2].position = { static_cast<float>(frame.width), static_cast<float>(frame.height)};
		m_vertices[3].position = { 0.0f, static_cast<float>(frame.height)};

		float left = frame.left + 0.0001f;
		float top = frame.top;
		float right = frame.left + frame.width;
		float bot = frame.top + frame.height;

		m_vertices[0].texCoords = {left, top};
		m_vertices[1].texCoords = {right, top};
		m_vertices[2].texCoords = {right, bot};
		m_vertices[3].texCoords = {left, bot};
	}
}

void jet::AnimatedSprite::setColor(const sf::Color& color)
{
	m_vertices[0].color = color;
	m_vertices[1].color = color;
	m_vertices[2].color = color;
	m_vertices[3].color = color;
}

void jet::AnimatedSprite::update(const sf::Time& time)
{
	if(!m_isPaused && m_currentAnimation != nullptr)
	{
		m_currentTime += time;
		if(m_currentTime >= m_frameTime)
		{
			m_currentTime = sf::microseconds(m_currentTime.asMicroseconds() % m_frameTime.asMicroseconds());

			if (m_currentFrame + 1 < m_currentAnimation->getFrameCount())
				m_currentFrame++;
			else
			{
				if (!m_isLoop)
					pause();
				else
				{
					m_currentFrame = 0;
				}
			}
				setFrame(m_currentFrame);
		}
	}
}

void jet::AnimatedSprite::setFlippedX(bool flip)
{
	m_isFlippedX = flip;
	if (flip)
		setScale(-1.0f, getScale().y);
	else
		setScale(1.0f, getScale().y);
}

bool jet::AnimatedSprite::isFlippedX() const
{
	return m_isFlippedX;
}

void jet::AnimatedSprite::setFlippedY(bool flip)
{
	m_isFlippedY = flip;
	if (flip)
		setScale(getScale().x, -1.0f);
	else
		setScale(getScale().x, 1.0f);
}

bool jet::AnimatedSprite::isFlippedY() const
{
	return m_isFlippedY;
}

bool jet::AnimatedSprite::isLooped() const
{
	return m_isLoop;
}

bool jet::AnimatedSprite::isPlaying() const
{
	return !m_isPaused;
}

void jet::AnimatedSprite::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if(m_currentAnimation)
	{
		states.transform *= getTransform();
		states.texture = m_texture;
		target.draw(m_vertices, states);
	}
}
