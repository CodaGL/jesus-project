#include "Widget.h"

#include <SFML/Graphics/RenderTarget.hpp>
#include "Math.h"
#include <iostream>
int i = 0;
jet::Widget::Widget()
{
	m_shape.setPosition(0, 0);
	m_width = m_height = 50;
	m_shape.setFillColor(sf::Color::Red);
	if(i == 0)
	{
		m_width = 200;
		m_height = 200;
		m_shape.setFillColor(sf::Color::Blue);
	}
	i++;
	std::cout << i;
	m_shape.setSize({ m_width, m_height });

	m_debugText.setPosition(0, m_height + 16);
}

jet::Widget* jet::Widget::getParent() const
{
	return m_parent;
}

void jet::Widget::update(const sf::Time& time)
{
	m_debugText.setString(std::to_string(getPosition().x) + " - " + std::to_string(getPosition().y));
}

void jet::Widget::setParent(Widget* widget)
{
	m_parent = widget;
}

void jet::Widget::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getRelative();
	target.draw(m_shape, states);

	target.draw(m_debugText, states);
}

const sf::Transform& jet::Widget::getRelative() const
{
	if (m_parent)
		return m_parent->getTransform() * getTransform();
	return getTransform();
}

void jet::Widget::setPosition(const sf::Vector2f& pos)
{
	sf::Vector2f p = pos;
	if(m_parent && m_isContained)
	{
		p = m_parent->getTransform().transformPoint(p);
		p.x = Math::clamp(p.x, 0, m_parent->getWidth() - getWidth());
		p.y = Math::clamp(p.y, 0, m_parent->getHeight() - getHeight());
	}
	sf::Transformable::setPosition(p);
}
