#pragma once
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <vector>
#include "Widget.h"

namespace jet
{

	class Gui : public virtual sf::Drawable, public virtual sf::Transformable
	{
		
	public:
		
		~Gui();

		void update(const sf::Time& time);
		
		void add(Widget* widget);
		
	private:
		std::vector<Widget*> m_widgets;


	private:
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	};
}
