#pragma once
#include "Entity.h"
#include <set>

namespace jet
{
	//DamageEntity are created to make damage to things/entities.
	class DamageEntity : public virtual Entity
	{
	public:
		DamageEntity(const Entity* source, float rangeX, float rangeY, int time = 10, int damage = 1);

		virtual void applyDamage();

		void tick() override;

		jet::EntityType getEntityType() const override;

	private:
		void onCollision(Entity* entity, ECollisionFlags collisionFlag) override;
		
		const jet::Entity* m_source;

		int m_timeTicks;
		int m_damage;

	protected:
		std::set<jet::Entity*> m_damagedEntities;
	};
}
