#pragma once
#include <string>
#include "tinyxml2.h"

struct LevelData;

class LevelReader
{
public:
	bool readLevel(const std::string& path, LevelData& levelData);

private:

	int m_gidEnemies;

	bool readLevelProperties(tinyxml2::XMLElement* root, LevelData& levelData);
	bool readGlobalGids(tinyxml2::XMLElement* root);

	bool readLevelLayers(tinyxml2::XMLElement* root, LevelData& levelData);

	bool readCollidableTileLayer(const std::string& layer, LevelData& levelData);
	bool readBackgroundTileLayer(const std::string& layer, LevelData& levelData);
	bool readMiddlegroundTileLayer(const std::string& layer, LevelData& levelData);
	bool readForegroundTileLayer(const std::string& layer, LevelData& levelData);

	bool readObjects(tinyxml2::XMLElement* root, LevelData& levelData);
	bool readLights(tinyxml2::XMLElement* group, LevelData& levelData);
	bool readEnemies(tinyxml2::XMLElement* group, LevelData& levelData);

	//static bool loadLights(const std::string&)

	bool loadLaddersTileLayer(const std::string& layer, LevelData& levelData);
};
