#pragma once

namespace Math
{
	inline float clamp(float value, float min, float max)
	{
		if (value > max)
			return max;
		if (value < min)
			return min;
		return value;
	}

	//Centered.
	inline bool isInsideView(sf::View& view, float x, float y, float sz_x, float sz_y)
	{
		float viewLeft = view.getCenter().x - view.getSize().x / 2.0f;
		float viewRight = view.getCenter().x + view.getSize().x / 2.0f;
		float viewTop = view.getCenter().y - view.getSize().y / 2.0f;
		float viewBot = view.getCenter().y + view.getSize().y / 2.0f;
		return (viewLeft <= (x + sz_x) && viewRight >= x - sz_x
			&& viewTop <= y + sz_y && viewBot >= y - sz_y);
		//return !(x + sz_x < viewLeft || y + sz_y < viewTop || x - sz_x > )
	}

	inline float lerp(float v0, float v1, float t) 
	{
		return (1 - t) * v0 + t * v1;
	}

	inline float distance(float x1, float y1, float x2, float y2)
	{
		float dst = sqrt(abs( (x1 * x1 - x2 * x2) + (y1 * y1 - y2 * y2)));
		return dst;
	}
}