#pragma once
#include "Animation.h"
#include <SFML/Graphics/Sprite.hpp>
#include "TileMap.h"
#include "AnimatedEntity.h"
#include "LevelData.h"
#include "Mob.h"
#include "EntityType.h"
#include "AnimatedSprite.h"


namespace
{
	const float kGravity = 1.0f / 16.0f;
	const float kMaxAccelerationY = 4.0f;

	const float kAccelerationX = 1.5f;//1.0f/128.0f;
	const float kMaxAccelerationX = 1.5f;

	const float kAccelerationXOnLadderSlowdown = 0.1f;

	const float kJumpAcceleration = 4.5f;//1.0f / 16.0f;
	const int kJumpTicks = 15; //How many ticks before falling down when jumping. (60 = 1 second before falling, for example)
}

namespace jet
{
	class Level;
	
	class Player : public jet::Mob
	{

	public:
		Player();
		~Player();

		Player(const Player& player) = delete;
		Player& operator= (const Player& player) = delete;

		void handleInput();

		void update(const sf::Time& time) override;
		void tick() override;
		void postTick() override;

		void processCollisions() override;

		void render(sf::RenderTarget& target) const override;
		void renderGUI() const;

		bool jump();
		void stopJump();

		bool attack();

		bool isOnLadder(bool checkFoot = false);
		
		void enterLadder();
		void exitLadder();

		void damage(int health, Entity* damagedBy) override;

		void onCollision(Entity* entity, ECollisionFlags collisionFlag) override;
	
		jet::EntityType getEntityType() const override;

	private:

		sf::Vector2f m_acceleration;
		
		int m_jumpTicks;
		
		int m_lastHitTicks;
		
		int m_attackTime; //It's keep count of the next attack time.

		bool m_isJumping;
		bool m_isOnLadder;
		bool m_isAttacking; //Resetted when animation is over.
		
	};


}