#pragma once
#include "Screen.h"
#include <SFML/Graphics/Sprite.hpp>
#include <vector>
#include "TileMap.h"

namespace jet
{
	class LevelEditorScreen : public Screen
	{
	public:
		void onEnter() override;
		virtual void onExit() override;

		void render(sf::RenderTarget& target) override;
		void tick() override;

	private:
		sf::Sprite m_currentTileset;
		
		uint8_t m_levelWidth;
		uint8_t m_levelHeight;

		uint8_t m_currentTileId;
		int m_currentLayer;

		sf::View view;
		float m_zoom;

		std::vector<std::vector<TileData>> m_layers;
		
		std::vector<sf::VertexArray> m_vertexMap;
		sf::VertexArray tilesGrid;

		void addLayer();

		void setTile(int x, int y, TileData id, bool rebuild = true);

		void renderGUI(sf::RenderTarget& target);

		void generateLayerMesh(int layer = 0);
		void generateGridMesh(const int mapWidth, const int mapHeight);
		
		void generateNewLevel(int levelWidth, int levelHeight);
	};
}
