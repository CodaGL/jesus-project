#include "ObjectFactory.h"


jet::Enemy* jet::ObjectFactory::createEnemy(jet::EnemyID enemy_id)
{
	Enemy* instance = nullptr;
	const auto& found = getInstance().m_enemies.find(enemy_id);
	if(found != getInstance().m_enemies.end())
	{
		instance = found->second();
	}
	return instance;
}

void jet::ObjectFactory::registerEnemy(jet::EnemyID enemy_id, EnemyConstructor constructor)
{
	if(getInstance().m_enemies.find(enemy_id) == getInstance().m_enemies.end())
	{
		getInstance().m_enemies[enemy_id] = constructor;
	}
}


//==========================================================================================
jet::UtilityRegister::UtilityRegister(jet::EnemyID eid, EnemyConstructor constructor)
{
	ObjectFactory::registerEnemy(eid, constructor);
}
